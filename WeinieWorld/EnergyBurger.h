//
//  EnergyBurger.h
//  WeinieWorld
//
//  Created by Rizwan on 3/20/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface EnergyBurger : CCNode {
    
}

@property (nonatomic, retain)CCProgressTimer *progressBar;

- (void)startProgress;
- (void)walkEnergyLoosing;

@end
