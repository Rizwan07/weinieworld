//
//  BackgroundLayer.m
//  WeinieWorld
//
//  Created by Rizwan on 4/16/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "BackgroundLayer.h"

@interface BackgroundLayer () {
    
    int _currentBgNum;
}

@property (nonatomic, retain) CCSprite *bgImage1;
@property (nonatomic, retain) CCSprite *bgImage2;
@property (nonatomic, retain) CCSprite *bgImage3;
@property (nonatomic, retain) CCSprite *bgImage4;

@end

@implementation BackgroundLayer

- (id)init {
    
    self = [super init];
    
    if (self != nil) {
        
        _currentBgNum = 1;
        
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        _bgImage4 = [CCSprite spriteWithFile:@"sky-568-3.png"];
        _bgImage4.position = ccp(size.width/2, size.height/2);//447...33
        [self addChild:_bgImage4 z:0];
        
        _bgImage3 = [CCSprite spriteWithFile:@"sky-568-2.png"];
        _bgImage3.position = ccp(size.width/2, size.height/2);//447...33
        [self addChild:_bgImage3 z:0];
        
        _bgImage2 = [CCSprite spriteWithFile:@"sky-568-1.png"];
        _bgImage2.position = ccp(size.width/2, size.height/2);//447...33
        [self addChild:_bgImage2 z:0];
        
        _bgImage1 = [CCSprite spriteWithFile:@"sky-568.png"];
        _bgImage1.position = ccp(size.width/2, size.height/2);//447...33
        [self addChild:_bgImage1 z:0];
        
        [self schedule:@selector(changeBg) interval:20.0f];
        
    }
    return self;
}

- (void)changeBg {
    switch (_currentBgNum) {
        case 1: {
            _currentBgNum ++;
            id fade = [CCFadeOut actionWithDuration:10.0];
            [_bgImage1 runAction:fade];
        }
            break;
        case 2: {
            _currentBgNum ++;
            id fade = [CCFadeOut actionWithDuration:10.0];
            [_bgImage2 runAction:fade];
        }
            break;
        case 3: {
            _currentBgNum ++;
            id fade = [CCFadeOut actionWithDuration:10.0];
            [_bgImage3 runAction:fade];
        }
            break;
        case 4: {
            _currentBgNum ++;
            id fade = [CCFadeOut actionWithDuration:10.0];
            [_bgImage4 runAction:fade];
        }
            break;
            
        default: {
            _currentBgNum = 1;
            [_bgImage1 runAction:[CCFadeIn actionWithDuration:2.0]];
            [_bgImage2 runAction:[CCFadeIn actionWithDuration:2.0]];
            [_bgImage3 runAction:[CCFadeIn actionWithDuration:2.0]];
            [_bgImage4 runAction:[CCFadeIn actionWithDuration:2.0]];
        }
            break;
    }
}

@end
