//
//  MenuScene.m
//  WeinieWorld
//
//  Created by Rizwan on 2/13/13.
//
//

#import "MenuScene.h"
#import "SettingScene.h"
#import "HelpScene.h"
#import "GamePlayScene.h"
#import "GamePlayLayer.h"
#import "DogAvatarScene.h"

@interface MenuScene () {
    
    CCScene *_gamePlay;
}

@end

@implementation MenuScene

+ (CCScene *)scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MenuScene *menuScene = [MenuScene node];
	
	// add layer as a child to scene
	[scene addChild: menuScene];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
- (id)init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
		
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        CCSprite *backgroundSrite = nil;
        
        //_gamePlay = [[GamePlayLayer alloc] init];
        //_gamePlay = [[GamePlayLayer scene] retain];
        //_gamePlay = [GamePlayLayer scene];
        //_gamePlay = [CCTransitionCrossFade transitionWithDuration:1.0 scene:[GamePlayLayer scene]];
        //_gamePlay = [[CCTransitionCrossFade alloc] initWithDuration:1.0f scene:[GamePlayLayer scene]];
        
        if (size.width > 480) {
            backgroundSrite = [CCSprite spriteWithFile:@"menu-background-568.png"];
        }
        else {
            backgroundSrite = [CCSprite spriteWithFile:@"menu-background.png"];
        }
        [backgroundSrite setPosition:ccp(size.width/2, size.height/2)];
        [self addChild:backgroundSrite];
        
        CCSprite *menuBarSrite = [CCSprite spriteWithFile:@"weinie-logo.png"];
        [menuBarSrite setPosition:ccp(size.width/2+6, size.height/2+100)];
        [self addChild:menuBarSrite z:1];
        
        CCMenuItem *menuItemPlay = [CCMenuItemImage itemWithNormalImage:@"btn-play-normal.png" selectedImage:@"btn-play-normal.png" block:^(id sender) {
            
            CCScene *layer = [CCTransitionCrossFade transitionWithDuration:1.0 scene:[GamePlayLayer scene]];
            [[CCDirector sharedDirector] replaceScene:layer];
            //[[CCDirector sharedDirector] replaceScene:_gamePlay];
        }];
        
        CCMenuItem *menuItemCustomize = [CCMenuItemImage itemWithNormalImage:@"btn-customize-normal.png" selectedImage:@"btn-customize-normal.png" block:^(id sender) {
            
            CCScene *layer = [CCTransitionCrossFade transitionWithDuration:1.0 scene:[DogAvatarScene scene]];
            [[CCDirector sharedDirector] replaceScene:layer];
        }];
        
        CCMenuItem *menuItemSetting = [CCMenuItemImage itemWithNormalImage:@"btn-setting-normal.png" selectedImage:@"btn-setting-normal.png" block:^(id sender) {
            
            CCScene *layer = [CCTransitionCrossFade transitionWithDuration:1.0 scene:[SettingScene scene]];
            [[CCDirector sharedDirector] replaceScene:layer];

        }];
        
        CCMenuItem *menuItemHelp = [CCMenuItemImage itemWithNormalImage:@"btn-help-normal.png" selectedImage:@"btn-help-normal.png" block:^(id sender) {
             
            CCScene *layer = [CCTransitionCrossFade transitionWithDuration:1.0 scene:[HelpScene scene]];
            [[CCDirector sharedDirector] replaceScene:layer];

        }];
        
        CCMenu *menu = [CCMenu menuWithItems:menuItemPlay, menuItemCustomize, menuItemSetting, menuItemHelp, nil];
        [menu alignItemsVerticallyWithPadding:0];
        [menu setPosition:ccp(size.width/2, size.height/2-44)];
        [self addChild:menu];
        
	}
	return self;
}

- (void)onEnter {
    
    CCTouchDispatcher *touchDispatcher = [[[CCTouchDispatcher alloc] init] autorelease];
    [touchDispatcher addTargetedDelegate:self priority:0 swallowsTouches:YES];
    [super onEnter];
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    
    return YES;
}

- (void)dealloc {
    [super dealloc];
//    [_gamePlay release];
}

@end
