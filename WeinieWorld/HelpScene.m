//
//  HelpScene.m
//  WeinieWorld
//
//  Created by Rizwan on 2/13/13.
//
//

#import "HelpScene.h"
#import "MenuScene.h"

@interface HelpScene () {
    
    UITextView *textView;
}

@end

@implementation HelpScene

+ (CCScene *)scene {
    
    CCScene *scene = [CCScene node];
    
    HelpScene *helpScene = [HelpScene node];
    
    [scene addChild:helpScene];
    return scene;
}

- (id)init {
    
    if (self = [super init]) {
        
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        CCSprite *backgroundSrite = nil;
        
        if (size.width > 480) {
            backgroundSrite = [CCSprite spriteWithFile:@"menu-background-568.png"];
        }
        else {
            backgroundSrite = [CCSprite spriteWithFile:@"menu-background.png"];
        }
        
        [backgroundSrite setPosition:ccp(size.width/2, size.height/2)];
        [self addChild:backgroundSrite];
        
        CCSprite *menuBarSrite = [CCSprite spriteWithFile:@"weinie-logo.png"];
        [menuBarSrite setPosition:ccp(size.width/2+6, size.height/2+100)];
        [self addChild:menuBarSrite z:1];
        

        CCSprite *helpBgSrite = [CCSprite spriteWithFile:@"bg-help.png"];
        [helpBgSrite setPosition:ccp(size.width/2, size.height/2-30)];
        [self addChild:helpBgSrite];

        CCMenuItem *item = [CCMenuItemImage itemWithNormalImage:@"btn-back.png" selectedImage:@"btn-back.png" block:^(id sender) {
            
            [textView removeFromSuperview];
            CCScene *scene = [CCTransitionFade transitionWithDuration:1.0 scene:[MenuScene scene]];
            [[CCDirector sharedDirector] replaceScene:scene];
        }];
        //item.scale = 0.8;
        CCMenu *menu = [CCMenu menuWithItems:item, nil];
        [menu setPosition:ccp(size.width/2-140, size.height/2+20)];
//        if (size.width > 480) {
//            [menu setPosition:ccp(180, 90)];
//        }
//        else {
//            [menu setPosition:ccp(150, 90)];
//        }
//        [menu setAnchorPoint:ccp(0, 0)];
//        [menu setRotation:-90];
        [self addChild:menu z:200];

        
        textView = [[UITextView alloc] initWithFrame:CGRectMake(size.width/2-150, size.height/2, 300, 130)];
        [textView setText:@"Keep the Weinie running for all time and get it eat as much food as you can. \nThe energy is maintained from the food the Weinie eats so you need to keep getting the food to continue. \nAvoid obstacles at all time otherwise you will be loosing energy whenever you hit them. \nSmall swipe up will have a small jump and for high jump, you need to swipe bigger. \nGet the jet pack to fly the weinie around and avoid obstacles for certain period of time. \nGet the Med pack to bypass the obstacles without reducing energy. \nGo to the avatar build to customize your Weinie the way you want. \nENJOY THE GAME !"];
        [textView setTextColor:[UIColor whiteColor]];
        [textView setBackgroundColor:[UIColor clearColor]];
        [textView setTextAlignment:NSTextAlignmentCenter];
        [textView setEditable:NO];
        [textView setDelegate:self];
        [[[CCDirector sharedDirector] view] addSubview:textView];

    }
    return self;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView1 {
    [textView resignFirstResponder];
    return NO;
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    return YES;
}

- (void)onEnter {
    
    CCTouchDispatcher *touchDispatcher = [[[CCTouchDispatcher alloc] init] autorelease];
    [touchDispatcher addTargetedDelegate:self priority:0 swallowsTouches:YES];
    [super onEnter];
}

@end
