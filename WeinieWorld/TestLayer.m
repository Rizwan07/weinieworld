//
//  TestLayer.m
//  WeinieWorld
//
//  Created by Rizwan on 4/10/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "TestLayer.h"


@interface TestLayer () {
    
    int particleType;
}

@end


@implementation TestLayer

/*+(CCScene *)scene {
    
    CCScene *scene = [CCScene node];
    
    TestLayer *layer = [TestLayer node];
    
    [scene addChild:layer];
    
    return scene;
}

- (id)init {
    
    self = [super init];
    
    if (self) {
        particleType = 1;
        [self runEffect];
        self.isTouchEnabled = YES;
    }
    return self;
}


-(void) runEffect {
    
    CCParticleSystem* system;
    switch (particleType)
    {
        case 1:
            system = [CCParticleExplosion node];
            break;
        case 2:
            system = [CCParticleFire node];
            break;
        case 3:
            system = [CCParticleFireworks node];
            break;
        case 4:
            system = [CCParticleFlower node];
            break;
        case 5:
            system = [CCParticleGalaxy node];
            break;
        case 6:
            system = [CCParticleMeteor node];
            break;
        case 7:
            system = [CCParticleRain node];
            break;
        case 8:
            system = [CCParticleSmoke node];
            break;
        case 9:
            system = [CCParticleSnow node];
            break;
        case 10:
            system = [CCParticleSpiral node];
            break;
        case 11:
            system = [CCParticleSun node];
            break;
        default:
            // do nothing
            break;
        
    }
    [system setPosition:ccp(240, 160)];
    [self addChild: system z:1 tag:1];
}


-(void) setNextParticleType {
    
    particleType++;
    if (particleType > 11)
    {
        particleType = 1;
    }
}

- (void)onEnter {
    
    CCTouchDispatcher *touchDispatcher = [[[CCTouchDispatcher alloc] init] autorelease];
    [touchDispatcher addTargetedDelegate:self priority:0 swallowsTouches:NO];
    
    [super onEnter];
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    
    return YES;
}

- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self setNextParticleType];
    [self removeChildByTag:1 cleanup:YES];
    [self runEffect];
    //particleType += 1;
    
    
}
*/

-(id) init
{
    return [self initWithTotalParticles:150];
}

-(id) initWithTotalParticles:(int)p {
    
    if( (self=[super initWithTotalParticles:p]) ) {
        // duration
        duration = kCCParticleDurationInfinity;
        
        //Auto remove
        self.autoRemoveOnFinish =YES;
        
        // Gravity Mode
        self.emitterMode = kCCParticleModeGravity;
        
        // Gravity Mode: gravity
        self.gravity = ccp(0,-30);
        
        // Gravity Mode:  radial
        self.radialAccel = 0;
        self.radialAccelVar = 0;
        
        //  Gravity Mode: speed of particles
        self.speed = 360;
        self.speedVar = 50;
        
        // emitter position
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        self.position = ccp(winSize.width/2, winSize.height/2);
        
        // angle
        angle = 270;
        angleVar = 10;
        
        // life of particles
        life = 0.75f;
        lifeVar = 1;
        
        // emits per frame
        emissionRate = totalParticles/life;
        
        // color of particles
        startColor.r = 0.76f;
        startColor.g = 0.25f;
        startColor.b = 0.12f;
        startColor.a = 1.0f;
        startColorVar.r = 0.0f;
        startColorVar.g = 0.0f;
        startColorVar.b = 0.0f;
        startColorVar.a = 0.0f;
        endColor.r = 0.0f;
        endColor.g = 0.0f;
        endColor.b = 0.0f;
        endColor.a = 1.0f;
        endColorVar.r = 0.0f;
        endColorVar.g = 0.0f;
        endColorVar.b = 0.0f;
        endColorVar.a = 0.0f;
        
        // size, in pixels
        startSize = 8.0f;
        startSizeVar = 2.0f;
        endSize = kCCParticleStartSizeEqualToEndSize;
        
        self.texture = [[CCTextureCache sharedTextureCache] addImage: @"fire.png"];
        
        // additive
        self.blendAdditive = YES;
    }
    
    return self;
}

@end
