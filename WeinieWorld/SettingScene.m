//
//  SettingScene.m
//  WeinieWorld
//
//  Created by Rizwan on 2/13/13.
//
//

#import "SettingScene.h"
#import "MenuScene.h"
#import "CCControlSlider.h"
#import "SimpleAudioEngine.h"

@interface SettingScene ()

- (void)helloToogle:(id)sender;

@end

@implementation SettingScene

+ (CCScene *)scene {
    
    CCScene *scene = [CCScene node];
    
    SettingScene *helpScene = [SettingScene node];
    
    [scene addChild:helpScene];
    return scene;
}

- (id)init {
    
    if (self = [super init]) {
        
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        
        CCSprite *backgroundSrite = nil;
        
        if (size.width > 480) {
            backgroundSrite = [CCSprite spriteWithFile:@"menu-background-568.png"];
        }
        else {
            backgroundSrite = [CCSprite spriteWithFile:@"menu-background.png"];
        }

        [backgroundSrite setPosition:ccp(size.width/2, size.height/2)];
        [self addChild:backgroundSrite];
        
        CCSprite *menuBarSrite = [CCSprite spriteWithFile:@"weinie-logo.png"];
        [menuBarSrite setPosition:ccp(size.width/2+6, size.height/2+100)];
        [self addChild:menuBarSrite z:1];
        
        CCMenuItem *menuItemSetting = [CCMenuItemImage itemWithNormalImage:@"setting-normal.png" selectedImage:@"setting-normal.png" block:^(id sender) {
            
            NSLog(@"play");
        }];
        [menuItemSetting setIsEnabled:NO];
        
        CCMenuItem *menuItemSound = [CCMenuItemImage itemWithNormalImage:@"btn-sound-normal.png" selectedImage:@"btn-sound-normal.png" block:^(id sender) {
            
            NSLog(@"setting");
        }];
        [menuItemSound setIsEnabled:NO];
        
        CCMenuItem *menuItemVolume = [CCMenuItemImage itemWithNormalImage:@"btn-volume-normal.png" selectedImage:@"btn-volume-normal.png" block:^(id sender) {
            
            NSLog(@"setting");
        }];
        [menuItemVolume setIsEnabled:NO];
        
        CCMenuItem *menuItemDone = [CCMenuItemImage itemWithNormalImage:@"btn-done-normal.png" selectedImage:@"btn-done-normal.png" block:^(id sender) {
            
            CCScene *layer = [CCTransitionCrossFade transitionWithDuration:1.0 scene:[MenuScene scene]];
            [[CCDirector sharedDirector] replaceScene:layer];

        }];
        
        CCMenu *menu = [CCMenu menuWithItems:menuItemSetting, menuItemSound, menuItemVolume, menuItemDone, nil];
        [menu alignItemsVerticallyWithPadding:0];
        [menu setPosition:ccp(size.width/2-1, size.height/2-46)];
        
        [self addChild:menu];
        
        CCMenuItemImage *sountItemOn = [CCMenuItemImage itemWithNormalImage:@"bell-normal.png" selectedImage:@"bell-normal.png"];
        CCMenuItemImage *soundItemOff = [CCMenuItemImage itemWithNormalImage:@"bell-selected.png" selectedImage:@"bell-selected.png"];
        
        CCMenuItemToggle *bellToggleMenu = [CCMenuItemToggle itemWithTarget:self selector:@selector(helloToogle:) items:sountItemOn, soundItemOff, nil];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isSoundOn"]) {
            [bellToggleMenu setSelectedIndex:0];
        }
        else {
            [bellToggleMenu setSelectedIndex:1];
        }
        [bellToggleMenu setScale:0.8];
        CCMenu *bellMenu = [CCMenu menuWithItems:bellToggleMenu, nil];
        [bellMenu setPosition:ccp(size.width/2+60, size.height/2-23)];
        [self addChild: bellMenu];
        
        CCControlSlider *slider = [CCControlSlider sliderWithBackgroundFile:@"volume-bar-back.png" progressFile:@"volume-bar-back.png" thumbFile:@"volume-slider.png"];
        slider.minimumValue = 0.0f;
        slider.maximumValue = 1.0f;
        
        [slider setPosition:ccp(size.width/2+30, size.height/2-80)];
        [slider setValue:[[SimpleAudioEngine sharedEngine] backgroundMusicVolume]];
        [slider addTarget:self action:@selector(valueChanged:) forControlEvents:CCControlEventValueChanged];
        
        [self addChild:slider z:100];
    }
    return self;
}

- (void)helloToogle:(id)sender {
    
    CCMenuItemToggle *toggle = (CCMenuItemToggle *)sender;
    
    switch ([toggle selectedIndex]) {
        case 0:
            NSLog(@"On");
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isSoundOn"];
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:[[NSUserDefaults standardUserDefaults] objectForKey:@"bgMusic"]];
            break;
        case 1:
            NSLog(@"Off");
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isSoundOn"];
            [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
            break;
        default:
            break;
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)valueChanged:(id)sender {
    
    CCControlSlider *slider = (CCControlSlider *)sender;
    
    [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:[slider value]];
    
    NSLog(@"sender tag %.1f", [slider value]);
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    return YES;
}

- (void)onEnter {
    
    CCTouchDispatcher *touchDispatcher = [[[CCTouchDispatcher alloc] init] autorelease];
    [touchDispatcher addTargetedDelegate:self priority:0 swallowsTouches:YES];
    [super onEnter];
}

@end
