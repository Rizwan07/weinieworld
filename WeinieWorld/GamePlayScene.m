//
//  GamePlayScene.m
//  WeinieWorld
//
//  Created by Rizwan on 2/13/13.
//
//

#import "GamePlayScene.h"
#import "MenuScene.h"

@implementation GamePlayScene

+ (CCScene *)scene {
    
    CCScene *scene = [CCScene node];
    
    GamePlayScene *playScene = [GamePlayScene node];
    [scene addChild:playScene];
    
    return scene;
}

- (id)init {
    
    if (self = [super init]) {
        
        CCSprite *backgroundImage = [CCSprite spriteWithFile:@"sky-568.png"];
        backgroundImage.position = ccp(284, 160);//447...33
        [self addChild:backgroundImage];

        
        [CCMenuItemFont setFontSize:20];
        
        CCMenuItem *item = [CCMenuItemFont itemWithString:@"back" block:^(id sender) {
            
            CCScene *scene = [CCTransitionFade transitionWithDuration:1.0 scene:[MenuScene scene]];
            [[CCDirector sharedDirector] replaceScene:scene];
        }];
        
        CCMenu *menu = [CCMenu menuWithItems:item, nil];
        [menu setPosition:ccp(20, 305)];
        
        [self addChild:menu];
        
        [self scheduleOnce:@selector(startAllAnimations) delay:1.0];

    }
    return self;
}

- (void)startAllAnimations {
    
    [self renderHorizonSprite];
    [self renderGreenrySprite];
    
    [self renderAnimation];
    
    [self renderFences];
    
    peopleSprite = [CCSprite spriteWithFile:@"audiance.png"];
    [self addChild:peopleSprite z:1];
    peopleSprite.position = ccp(peopleSprite.contentSize.width/2, 160);//447...33
    [self schedule:@selector(renderPeople:) interval:0.1];
    
    [self schedule:@selector(renderCloud:) interval:2.0];

}

- (void)renderAnimation {
    
    // Load up the frames of our animation
    
    dogSprite = [CCSprite spriteWithFile:@"Frame 1.png"];
    dogSprite.position = ccp(150, 80);

    [self addChild:dogSprite z:300];
    
    CCAnimation *anime= [CCAnimation animation];
    
    anime.delayPerUnit = 0.1;
    for(int i = 1; i <= 3; i++){
        [anime addSpriteFrameWithFilename:[NSString  stringWithFormat:@"Frame %d.png", i]];
    }
    id animeAction = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:anime]];
    [dogSprite runAction:animeAction];
    
}

- (void)jumpDogi {
    
    dogSprite = [CCSprite spriteWithFile:@"Frame 1.png"];
    dogSprite.position = ccp(150,80);
    [self addChild:dogSprite];
    
    id jump = [CCJumpBy actionWithDuration:1 position:ccp(0,100) height:40 jumps:3];
    id action = [CCSequence actions: jump,[CCScaleTo actionWithDuration:1.0-(1.0*0.6) scale:0.0], nil];
    [dogSprite runAction:action];

    
//    CCSprite *move = [CCSprite spriteWithFile:@"image1.png" rect:CGRectMake(0, 0, 30, 30)];
//    move.position = ccp(winSize.width/2,70);
//    [self addChild:moveBanana];
//    
//    id jump = [CCJumpBy actionWithDuration:1 position:ccp(0,100) height:40 jumps:3];
//    id action = [CCSequence actions: jump,[CCScaleTo actionWithDuration:realMoveDuration-(realMoveDuration*0.6) scale:0.0], nil];
//    [move runAction:action];

}

- (void)renderPeople:(ccTime)dt {
    
    if (peopleSprite.position.x < 5) {
        peopleSprite.position = ccp(peopleSprite.contentSize.width/2, 160);
    }
    else {
        CGPoint pos;
        pos = peopleSprite.position;
        pos.x -= 5;
        peopleSprite.position = pos;
    }
    
}

- (void)renderGreenrySprite {
    
    CCSprite *greenSprite = [CCSprite spriteWithFile:@"greenry.png"];
    greenSprite.position = ccp(568-190, 50);
    [self addChild:greenSprite z:2];
    
    ccTime actualDuration = 2;
    
    id actionMoveTo = [CCMoveTo actionWithDuration:actualDuration position:ccp(0, 50)];
    id initPosition = [CCPlace actionWithPosition:ccp(568-190, 50)];
    id rptFrvr1 = [CCRepeatForever actionWithAction:[CCSequence actions:actionMoveTo, initPosition, nil]];
    [greenSprite runAction:rptFrvr1];
    
}

- (void)renderHorizonSprite {
    
    CCSprite *character = [CCSprite spriteWithFile:@"buildings.png"];
    character.position = ccp(568, 200);
    [self addChild:character];
    
    ccTime actualDuration = 10;
    
    id actionMoveTo = [CCMoveTo actionWithDuration:actualDuration position:ccp(0, 200)];
    id initPosition = [CCPlace actionWithPosition:ccp(568, 200)];
    id rptFrvr1 = [CCRepeatForever actionWithAction:[CCSequence actions:actionMoveTo, initPosition, nil]];
    [character runAction:rptFrvr1];
    
}

- (void)renderFences {
    
    CCSprite *fenceFrontSprite = [CCSprite spriteWithFile:@"front-fence1.png"];
    CCSprite *fencebackSprite = [CCSprite spriteWithFile:@"back-fence1.png"];
    fenceFrontSprite.position = ccp(284, 20);
    fencebackSprite.position = ccp(284, 150);
    
    [self addChild:fenceFrontSprite z:301];
    [self addChild:fencebackSprite z:301];
    
    CCAnimation *animationFrontFence= [CCAnimation animation];
    CCAnimation *animationbackFence= [CCAnimation animation];
    
    animationFrontFence.delayPerUnit = 0.1;
    animationbackFence.delayPerUnit = 0.1;
    for(int i = 1; i <= 2; i++){
        [animationFrontFence addSpriteFrameWithFilename:[NSString  stringWithFormat:@"front-fence%d.png", i]];
        [animationbackFence addSpriteFrameWithFilename:[NSString  stringWithFormat:@"back-fence%d.png", i]];
    }
    id animationFrontAction = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:animationFrontFence]];
    id animationBackAction = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:animationbackFence]];
    [fenceFrontSprite runAction:animationFrontAction];
    [fencebackSprite runAction:animationBackAction];

}

- (void) renderCloud: (ccTime)dt {
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    int tagNum =  (arc4random()%2+1);
	NSString *strCloud = [NSString stringWithFormat:@"Cloud_%i_SC.png",tagNum];
    
    CCSprite *cloud = [CCSprite spriteWithFile:strCloud];
    //cloud.scale = 1.4 + 0.01 * (arc4random()%10);
	[self addChild:cloud z:tagNum];
    
	cloud.position = ccp(winSize.width + cloud.contentSize.width/2 * cloud.scale, 250 + (arc4random()% 4) * 20);
	
	int duration = 10 + arc4random() % 20;
	id actionMove = [CCMoveTo actionWithDuration:duration position:ccp(-cloud.contentSize.width/2 * cloud.scale, cloud.position.y)];
    
	id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(cloudMoveDone:)];
	[cloud runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
    
}

- (void) cloudMoveDone: (id) sender {
	CCSprite *cloud = (CCSprite *)sender;
	[self removeChild: cloud cleanup: YES];
}

- (void)onEnter {
    
    CCTouchDispatcher *touchDispatcher = [[[CCTouchDispatcher alloc] init] autorelease];
    [touchDispatcher addTargetedDelegate:self priority:0 swallowsTouches:YES];
    
    [super onEnter];
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    
    return YES;
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self jumpDogi];
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
    
    [self jumpDogi];
}

@end
