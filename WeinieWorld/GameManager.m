//
//  GameManager.m
//  WeinieWorld
//
//  Created by Rizwan on 3/20/13.
//
//

#import "GameManager.h"

@implementation GameManager

static GameManager *gameManager = nil;

+ (id)sharedGameManager {
    
    if (gameManager == nil) {
        gameManager = [[GameManager alloc] init];
    }
    return gameManager;
}

+ (CCSprite *)getRandomObstacleSprite {
    
    short rand = arc4random()%14+1;
    
    CCSprite *obstacleSprite;
    
    switch (rand) {
        case 1:
            obstacleSprite = [CCSprite spriteWithFile:@"hole.png"];
            obstacleSprite.tag = 1200;
            break;
        case 2:
            obstacleSprite = [CCSprite spriteWithFile:@"obstacle-2.png"];
            obstacleSprite.tag = 100;
            break;
        case 3:
            obstacleSprite = [CCSprite spriteWithFile:@"cactus-1.png"];
            obstacleSprite.tag = 100;
            break;
        case 4:
            obstacleSprite = [CCSprite spriteWithFile:@"cactus-2.png"];
            obstacleSprite.tag = 100;
            break;
        case 5:
            obstacleSprite = [CCSprite spriteWithFile:@"slap.png"];
            obstacleSprite.tag = 100;
            break;
        case 6:
            obstacleSprite = [CCSprite spriteWithFile:@"stone.png"];
            obstacleSprite.tag = 100;
            break;
        case 7:
            obstacleSprite = [CCSprite spriteWithFile:@"pond.png"];
            obstacleSprite.tag = 100;
            break;
        case 8:
            obstacleSprite = [CCSprite spriteWithFile:@"boll.png"];
            obstacleSprite.tag = 100;
            break;
        case 9:
            obstacleSprite = [CCSprite spriteWithFile:@"wooden-crate.png"];
            obstacleSprite.tag = 100;
            break;
        case 10:
            obstacleSprite = [CCSprite spriteWithFile:@"bushes.png"];
            obstacleSprite.tag = 100;
            break;
        case 11:
            obstacleSprite = [CCSprite spriteWithFile:@"plant-1.png"];
            obstacleSprite.tag = 100;
            break;
        case 12:
            obstacleSprite = [CCSprite spriteWithFile:@"plant-2.png"];
            obstacleSprite.tag = 100;
            break;
        case 13:
            obstacleSprite = [CCSprite spriteWithFile:@"cone.png"];
            obstacleSprite.tag = 100;
            break;
        case 14:
            obstacleSprite = [CCSprite spriteWithFile:@"obstacle-1.png"];
            obstacleSprite.tag = 100;
            break;
        default:
            obstacleSprite = [CCSprite spriteWithFile:@"obstacle-1.png"];
            obstacleSprite.tag = 100;
            break;
    }
    
    return obstacleSprite;
}

+ (CCSprite *)getRandomBonusSprite {
    
    int rand = arc4random()%8+1;
    
    CCSprite *bonusSprite;
    
    switch (rand) {
        case 1:
            bonusSprite = [CCSprite spriteWithFile:@"fish.png"];
            [bonusSprite setTag:200];
            break;
        case 2:
            bonusSprite = [CCSprite spriteWithFile:@"leg-piece.png"];
            [bonusSprite setTag:201];
            break;
        case 3:
            bonusSprite = [CCSprite spriteWithFile:@"chocolate-cone.png"];
            [bonusSprite setTag:202];
            break;
        case 4:
            bonusSprite = [CCSprite spriteWithFile:@"pizza.png"];
            [bonusSprite setTag:203];
            break;
        case 5:
            bonusSprite = [CCSprite spriteWithFile:@"bone.png"];
            [bonusSprite setTag:204];
            break;
        case 6:
            bonusSprite = [CCSprite spriteWithFile:@"burger.png"];
            [bonusSprite setTag:205];
            break;
        case 7:
            bonusSprite = [CCSprite spriteWithFile:@"meat.png"];
            [bonusSprite setTag:206];
            break;
        case 8:
            bonusSprite = [CCSprite spriteWithFile:@"hot-dog.png"];
            [bonusSprite setTag:207];
            break;
        default:
            bonusSprite = [CCSprite spriteWithFile:@"fish.png"];
            [bonusSprite setTag:200];
            break;
    }
    return bonusSprite;

}

@end
