//
//  EnergyBurger.m
//  WeinieWorld
//
//  Created by Rizwan on 3/20/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "EnergyBurger.h"

@interface EnergyBurger ()

@property (nonatomic, retain)CCLabelTTF *energyPercent;

@end

@implementation EnergyBurger

- (id)init {
    
    self = [super init];
    
    if (self) {
        
        CCSprite *burgerImage = [CCSprite spriteWithFile:@"energy-burger.png"];
        burgerImage.position = ccp(70, 280);//447...33
        [self addChild:burgerImage z:1];
        
        CCLabelTTF *energyLbl = [CCLabelTTF labelWithString:@"Energy" fontName:@"Helvetica-Bold" fontSize:15];
        [energyLbl setPosition:ccp(70, 300)];
        [self addChild:energyLbl z:2];

    }
    return self;
    
}

- (void)startProgress {
    
    _energyPercent = [CCLabelTTF labelWithString:@"0%" fontName:@"Helvetica-Bold" fontSize:14];
    [_energyPercent setPosition:ccp(70, 260)];
    [self addChild:_energyPercent z:2];
    
    [self schedule:@selector(changeEnergyString:) interval:0.1];
    
    CCProgressTo *to1 = [CCProgressTo actionWithDuration:2 percent:100];
    CCSprite *energyBarSprite = [CCSprite spriteWithFile:@"energy-bar.png"];
    _progressBar = [CCProgressTimer progressWithSprite:energyBarSprite];
    _progressBar.type = kCCProgressTimerTypeBar;
    _progressBar.midpoint = ccp(0, 0);
    _progressBar.barChangeRate = ccp(1, 0);
    [_progressBar setPosition:ccp(70,276)];
    [self addChild:_progressBar z:3];
    [_progressBar runAction:to1];

}

- (void)changeEnergyString:(ccTime *)time {
    
    float timePercent = _progressBar.percentage;
    
    if (timePercent >= 100.0) {
        [_energyPercent setString:@"100%"];
        [self unschedule:@selector(changeEnergyString:)];
        
        [self schedule:@selector(walkEnergyLoosing) interval:0.5];
    }
    else {
        [_energyPercent setString:[NSString stringWithFormat:@"%.f%%", timePercent]];
    }
}

- (void)walkEnergyLoosing {
    
    
    [_energyPercent setString:[NSString stringWithFormat:@"%.f%%", _progressBar.percentage]];
    _progressBar.percentage --;
}

@end
