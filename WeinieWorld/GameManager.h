//
//  GameManager.h
//  WeinieWorld
//
//  Created by Rizwan on 3/20/13.
//
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface GameManager : NSObject


+ (id)sharedGameManager;

+ (CCSprite *)getRandomObstacleSprite;
+ (CCSprite *)getRandomBonusSprite;

@end
