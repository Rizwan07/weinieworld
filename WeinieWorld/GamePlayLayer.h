//
//  GamePlayLayer.h
//  WeinieWorld
//
//  Created by Rizwan on 2/18/13.
//
//

#import "cocos2d.h"
#import "chipmunk.h"

@interface GamePlayLayer : CCLayer {
    
    CCSprite *peopleSprite;
    CCSprite *dogSprite;
    CCSprite *dogJumpSprite;
    CCSprite *dogShadow;
    CCSprite *dogJumpShadow;
    CCSprite *shieldSprite1;
    CCSprite *shieldSprite2;
    CCSprite *wingSprite;
    
    CCSprite *dogHeadStarsSprite;
    CCSprite *dogJetPack;
    
    NSMutableArray *arrBonus;
    NSMutableArray *arrObstacle;
    
    CCMenu *mainMenu;
    
    BOOL ismove;
    
    CGPoint startPoint;
    CGPoint endPoint;
    
    int bonusNum;
    
    CCSprite *fenceFrontSprite;
    CCSprite *fencebackSprite;
    
}

+ (CCScene *)scene;

@end
