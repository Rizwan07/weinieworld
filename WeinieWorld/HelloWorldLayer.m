//
//  HelloWorldLayer.m
//  WeinieWorld
//
//  Created by Rizwan on 2/4/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#pragma mark - HelloWorldLayer

// HelloWorldLayer implementation
@implementation HelloWorldLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
		
        [self renderHorizonSprite];
        
        peopleSprite = [CCSprite spriteWithFile:@"Horizon_MV.png"];
        [self addChild:peopleSprite z:1];
        peopleSprite.position = ccp(peopleSprite.contentSize.width/2, 160);//447...33
        [self schedule:@selector(renderPeople:) interval:0.1];
        
        [self schedule:@selector(renderCloud:) interval:2.0];
        
        
        
		// create and initialize a Label
//		CCLabelTTF *label = [CCLabelTTF labelWithString:@"Hello World" fontName:@"Marker Felt" fontSize:64];
//
//		// ask director for the window size
//		CGSize size = [[CCDirector sharedDirector] winSize];
//	
//		// position the label on the center of the screen
//		label.position =  ccp( size.width /2 , size.height/2 );
//		
//		// add the label as a child to this Layer
//		[self addChild: label];
//		
//		
//		
//		//
//		// Leaderboards and Achievements
//		//
//		
//		// Default font size will be 28 points.
//		[CCMenuItemFont setFontSize:28];
//		
//		// Achievement Menu Item using blocks
//		CCMenuItem *itemAchievement = [CCMenuItemFont itemWithString:@"Achievements" block:^(id sender) {
//			
//			
//			GKAchievementViewController *achivementViewController = [[GKAchievementViewController alloc] init];
//			achivementViewController.achievementDelegate = self;
//			
//			AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
//			
//			[[app navController] presentModalViewController:achivementViewController animated:YES];
//			
//			[achivementViewController release];
//		}];
//
//		// Leaderboard Menu Item using blocks
//		CCMenuItem *itemLeaderboard = [CCMenuItemFont itemWithString:@"Leaderboard" block:^(id sender) {
//			
//			
//			GKLeaderboardViewController *leaderboardViewController = [[GKLeaderboardViewController alloc] init];
//			leaderboardViewController.leaderboardDelegate = self;
//			
//			AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
//			
//			[[app navController] presentModalViewController:leaderboardViewController animated:YES];
//			
//			[leaderboardViewController release];
//		}];
//		
//		CCMenu *menu = [CCMenu menuWithItems:itemAchievement, itemLeaderboard, nil];
//		
//		[menu alignItemsHorizontallyWithPadding:20];
//		[menu setPosition:ccp( size.width/2, size.height/2 - 50)];
//		
//		// Add the menu to the layer
//		[self addChild:menu];

	}
	return self;
}

- (void)renderPeople:(ccTime)dt {
    
    if (peopleSprite.position.x < 5) {
        peopleSprite.position = ccp(peopleSprite.contentSize.width/2, 160);
    }
    else {
        CGPoint pos;
        pos = peopleSprite.position;
        pos.x -= 5;
        peopleSprite.position = pos;
    }

}

- (void)renderHorizonSprite {
    
    CCSprite *character = [CCSprite spriteWithFile:@"Horizon_SA.png"];
    character.position = ccp(480, 200);
    
    [self addChild:character];
    
    ccTime actualDuration = 10;
    
    id actionMoveTo = [CCMoveTo actionWithDuration:actualDuration position:ccp(0, 200)];
    id initPosition = [CCPlace actionWithPosition:ccp(480,200)];
    id rptFrvr1 = [CCRepeatForever actionWithAction:[CCSequence actions:actionMoveTo, initPosition, nil]];
    [character runAction:rptFrvr1];

}

- (void) renderCloud: (ccTime)dt {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    int tagNum =  (arc4random()%2+1);
	NSString *strCloud = [NSString stringWithFormat:@"Cloud_%i_SC.png",tagNum];
    
    CCSprite *cloud = [CCSprite spriteWithFile:strCloud];
    cloud.scale = 1.4 + 0.01 * (arc4random()%10);
	[self addChild:cloud z:tagNum];
    
	cloud.position = ccp(-cloud.contentSize.width/2 * cloud.scale, 250 + (arc4random()% 4) * 20);
	
	int duration = 10 + arc4random() % 20;
	id actionMove = [CCMoveTo actionWithDuration:duration position:ccp(winSize.width + cloud.contentSize.width/2 * cloud.scale, cloud.position.y)];
    
    
    
	id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(cloudMoveDone:)];
	[cloud runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
    
}

- (void) cloudMoveDone: (id) sender {
	CCSprite *cloud = (CCSprite *)sender;
	[self removeChild: cloud cleanup: YES];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}

#pragma mark GameKit delegate

-(void) achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

-(void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}
@end
