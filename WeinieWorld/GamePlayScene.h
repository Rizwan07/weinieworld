//
//  GamePlayScene.h
//  WeinieWorld
//
//  Created by Rizwan on 2/13/13.
//
//

#import "cocos2d.h"

@interface GamePlayScene : CCScene <CCTargetedTouchDelegate, CCStandardTouchDelegate> {
    
    CCSprite *peopleSprite;
    CCSprite *dogSprite;
}

+ (CCScene *)scene;

@end
