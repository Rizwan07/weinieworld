//
//  Coin.m
//  WeinieWorld
//
//  Created by Rizwan on 3/20/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "BonusHeader.h"

@interface BonusHeader ()

@property (nonatomic, retain) CCSprite *spriteCoin;

@end

@implementation BonusHeader


- (id)initWithPosition:(CGPoint )point andFileName:(NSString *)fileName {
    
    self = [super init];
    if (self) {
        
        _spriteCoin = [CCSprite spriteWithFile:fileName];
        //[_spriteCoin setPosition:ccp(180, 300)];
        [_spriteCoin setPosition:point];
        [_spriteCoin setTag:500];
        _spriteCoin.scale = 0.6;
        [self addChild:_spriteCoin z:1];
        
        [_spriteCoin runAction:[CCRepeatForever actionWithAction:[CCRotateBy actionWithDuration:4.0 angle:360]]];
        
//        id rotate = [CCRotateTo actionWithDuration:2.0 angle:360];
//        id repeat = [CCRepeatForever actionWithAction:rotate];
//        [_spriteCoin runAction:repeat];
        
        
        _lblCoin = [CCLabelTTF labelWithString:@"0" fontName:@"Helvetica-Bold" fontSize:16];
        //[_lblCoin setPosition:ccp(155, 300)];
        [_lblCoin setPosition:ccp(point.x-25, point.y)];
        [self addChild:_lblCoin z:1];
        
        [self setPosition:ccp(0, 0)];
        [self setAnchorPoint:ccp(0, 0)];
        
    }
    return self;
}

- (void)spinCoin
{
    
    if (_spriteCoin.scaleX > 0.3 && _spriteCoin.tag == 500) {
        _spriteCoin.scaleX -= 0.2;
    }
    else {
        _spriteCoin.tag = 600;
        _spriteCoin.scaleX += 0.2;
        if (_spriteCoin.scaleX == 1.0) {
            _spriteCoin.tag = 500;
        }
    }
}


@end
