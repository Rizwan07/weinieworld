//
//  Coin.h
//  WeinieWorld
//
//  Created by Rizwan on 3/20/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface BonusHeader : CCNode {
    
}

@property (nonatomic, retain)CCLabelTTF *lblCoin;

- (id)initWithPosition:(CGPoint )point andFileName:(NSString *)fileName;

@end
