//
//  IntroLayer.m
//  WeinieWorld
//
//  Created by Rizwan on 2/4/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//


// Import the interfaces
#import "IntroLayer.h"
#import "HelloWorldLayer.h"
#import "MenuScene.h"
#import "TestLayer.h"
#import "SimpleAudioEngine.h"

#pragma mark - IntroLayer

// HelloWorldLayer implementation
@implementation IntroLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	IntroLayer *layer = [IntroLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// 
-(void) onEnter
{
	[super onEnter];

	// ask director for the window size
	CGSize size = [[CCDirector sharedDirector] winSize];
    
    CCSprite *backgroundSrite = nil;
    
    if (size.width > 480) {
        backgroundSrite = [CCSprite spriteWithFile:@"Default-568h@2x.png"];
    }
    else {
        
        if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0)) {
            backgroundSrite = [CCSprite spriteWithFile:@"Default@2x.png"];
        } else {
            backgroundSrite = [CCSprite spriteWithFile:@"Default.png"];
        }
        
    }
    
    [backgroundSrite setPosition:ccp(size.width/2, size.height/2)];
    [self addChild:backgroundSrite];
    
    [backgroundSrite setRotation:-90];
    
//    CCSprite *menuBarSrite = [CCSprite spriteWithFile:@"menu-bar.png"];
//    [menuBarSrite setPosition:ccp(size.width/2, size.height/2)];
//    [self addChild:menuBarSrite];

    
//	CCSprite *background;
//	
//	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
//		background = [CCSprite spriteWithFile:@"Default.png"];
//		//background.rotation = 90;
//	} else {
//		background = [CCSprite spriteWithFile:@"Default-Landscape~ipad.png"];
//	}
//	background.position = ccp(size.width/2, size.height/2);
//
//	// add the label as a child to this Layer
//	[self addChild: background];
	
	// In one second transition to the new scene
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey:@"avatarHead"] == nil) {
        
        [defaults setObject:@"avatar-head1.png" forKey:@"avatarHead"];
        [defaults setObject:@"avatar-body1.png" forKey:@"avatarBody"];
        [defaults setObject:@"avatar-ear1.png" forKey:@"avatarEar"];
        [defaults setObject:@"avatar-scarf1.png" forKey:@"avatarScarf"];
        [defaults setObject:@"avatar-back-legs1.png" forKey:@"avatarBLeg"];
        [defaults setObject:@"avatar-front-legs1.png" forKey:@"avatarFLeg"];
        [defaults setObject:@"avatar-tail1.png" forKey:@"avatarTail"];
        
        [defaults synchronize];
    }
    
	[self scheduleOnce:@selector(makeTransition:) delay:1];
}

-(void) makeTransition:(ccTime)dt
{
    
//    CCParticleSystem *emitter = [CCParticleSmoke node];
//    
//    //set the location of the emitter
//    emitter.position = ccp(240, 160);
//    emitter.autoRemoveOnFinish = YES;
//    
//    //set size of particle animation
//    //emitter.scale = 0.5;
//    
//    emitter.emitterMode = kCCParticleModeGravity;
//    
//    emitter.emissionRate = 200;
//    //emitter.angle = 0;
//    //set an Image for the particle
//    emitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"jet-fire.png"];
//    
//    [emitter setLife:0.5f];
//    emitter.speed = 150;
//    emitter.speedVar = 50;
//    
//    emitter.startSize = 100.0f;
//    emitter.startSizeVar = 1.0f;
//    
//    emitter.startRadius = 10.0f;
//    emitter.endRadius = 100.0f;
    
//    CCParticleExplosion* emitter = [CCParticleExplosion node];
//    emitter.autoRemoveOnFinish = YES;
//    emitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"jet-fire.png"];
//    emitter.startSize = 5.0f;
//    emitter.endSize = 1.0f;
//    emitter.duration = 0.2f;
//    emitter.speed = 100.0f;
//    emitter.anchorPoint = ccp(0.5f,0.5f);
//    emitter.position = ccp(240, 160);
//    
//    [self addChild: emitter z:100];
//    
//    
//    return;
    
    CCScene *layer = [CCTransitionCrossFade transitionWithDuration:1.0 scene:[MenuScene scene]];
    //CCScene *layer = [CCTransitionCrossFade transitionWithDuration:1.0 scene:[TestLayer scene]];
    [[CCDirector sharedDirector] replaceScene:layer];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (![defaults boolForKey:@"isFirstTime"]) {
        [defaults setBool:YES forKey:@"isFirstTime"];
        [defaults setBool:YES forKey:@"isSoundOn"];
        [defaults setObject:@"bg-music-1.mp3" forKey:@"bgMusic"];
        [defaults synchronize];
    }
    
    if ([defaults boolForKey:@"isSoundOn"]) {
        
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:[defaults objectForKey:@"bgMusic"]];
    }

	//[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[HelloWorldLayer scene] withColor:ccGRAY]];
}

@end
