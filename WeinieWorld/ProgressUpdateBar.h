//
//  Protocol.h
//  WeinieWorld
//
//  Created by Rizwan on 2/26/13.
//
//

#import <Foundation/Foundation.h>

@protocol ProgressUpdateBar <NSObject>

- (void)updateProgress:(int)percentage;

@end
