//
//  GamePlayLayer.m
//  WeinieWorld
//
//  Created by Rizwan on 2/18/13.
//
//

#import "GamePlayLayer.h"
#import "MenuScene.h"
#import "EnergyBurger.h"
#import "BonusHeader.h"
#import "BackgroundLayer.h"
#import "SimpleAudioEngine.h"

@interface GamePlayLayer () {
    
    int bunchHeightNum;
    float dogJumpDuration;
    
    BonusHeader *bHBone;
    BonusHeader *bHFish;
    BonusHeader *bHLegPiece;
    BonusHeader *bHPizza;
    
    CCSprite *greenSprite;
    
    int numOfBones;
    int numOfFishes;
    int numOfLegPieces;
    int numOfPizzas;
    
    NSMutableArray *arrToRemoveObj;
    
    ccTime bonusSpriteMoveDuration;
    ccTime obstacleSpriteMoveDuration;
    
    int numOfTimesRocket;
    
    CCLabelTTF *lblNoOfJumpsFromHurdles;
    
    BOOL isJetPackOn;
    BOOL isShieldOn;
    
    BOOL isGameOver;
    
    CCSprite *_spriteObstacleNotRemove;
}

@property (nonatomic, assign)int numHead;
@property (nonatomic, assign)int numBody;
@property (nonatomic, assign)int numEar;
@property (nonatomic, assign)int numScarf;
@property (nonatomic, assign)int numbLeg;
@property (nonatomic, assign)int numfLeg;
@property (nonatomic, assign)int numTail;

@property (nonatomic, assign)int randNumOfBonus;
@property (nonatomic, assign)int bonusPattern;

@property (nonatomic, retain)EnergyBurger *burder;

@end

@implementation GamePlayLayer

+ (CCScene *)scene {
    
    CCScene *scene = [CCScene node];
    
    GamePlayLayer *playScene = [GamePlayLayer node];
    [scene addChild:playScene];
    
    return scene;
}

- (id)init {
    
    if (self = [super init]) {
        
        bonusNum = 0;
        bunchHeightNum = 1;
        _randNumOfBonus = 5;
        numOfBones = 0;
        numOfFishes = 0;
        numOfLegPieces = 0;
        numOfPizzas = 0;
        numOfTimesRocket = 0;
        
        bonusSpriteMoveDuration = 2.5;
        obstacleSpriteMoveDuration = 1.8;
        
        dogJumpDuration = 0.0;
        
        isJetPackOn = NO;
        isShieldOn = NO;
        isGameOver = NO;
        
        arrToRemoveObj = [[NSMutableArray alloc] initWithCapacity:0];
        
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        arrBonus    = [[NSMutableArray alloc] initWithCapacity:0];
        arrObstacle = [[NSMutableArray alloc] initWithCapacity:0];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSString *avatarHeadStr = [defaults stringForKey:@"avatarHead"];
        NSString *avatarBodyStr = [defaults objectForKey:@"avatarBody"];
        NSString *avatarEarStr = [defaults objectForKey:@"avatarEar"];
        NSString *avatarScarfStr = [defaults objectForKey:@"avatarScarf"];
        NSString *avatarBLegStr = [defaults objectForKey:@"avatarBLeg"];
        NSString *avatarFLegStr = [defaults objectForKey:@"avatarFLeg"];
        NSString *avatarTailStr = [defaults objectForKey:@"avatarTail"];
        
        _numHead = [self selectedIndexDogHeadWithName:avatarHeadStr];
        _numBody = [self selectedIndexDogHeadWithName:avatarBodyStr];
        _numEar = [self selectedIndexDogHeadWithName:avatarEarStr];
        _numScarf = [self selectedIndexDogHeadWithName:avatarScarfStr];
        _numbLeg = [self selectedIndexDogHeadWithName:avatarBLegStr];
        _numfLeg = [self selectedIndexDogHeadWithName:avatarFLegStr];
        _numTail = [self selectedIndexDogHeadWithName:avatarTailStr];
        
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"shadow.plist"];
        
        BackgroundLayer *bgLayer = [[BackgroundLayer alloc] init];
        [bgLayer setPosition:ccp(0, 0)];
        [self addChild:bgLayer z:0];
        [bgLayer release];
        
        _burder = [[EnergyBurger alloc] init];
        _burder.anchorPoint = ccp(0, 0);
        _burder.position = ccp(0, 0);
        [self addChild:_burder z:2];
        
        bHBone = [[BonusHeader alloc] initWithPosition:ccp(180, 300) andFileName:@"bone.png"];
        [self addChild:bHBone z:2];
        
        bHFish = [[BonusHeader alloc] initWithPosition:ccp(240, 300) andFileName:@"fish.png"];
        [self addChild:bHFish z:2];
        
        bHLegPiece = [[BonusHeader alloc] initWithPosition:ccp(300, 300) andFileName:@"leg-piece.png"];
        [self addChild:bHLegPiece z:2];
        
        bHPizza = [[BonusHeader alloc] initWithPosition:ccp(360, 300) andFileName:@"pizza.png"];
        [self addChild:bHPizza z:2];
        
        lblNoOfJumpsFromHurdles = [CCLabelTTF labelWithString:@"0" fontName:@"Helvetica" fontSize:12];
        [lblNoOfJumpsFromHurdles setAnchorPoint:ccp(0, 0)];
        [lblNoOfJumpsFromHurdles setPosition:ccp(180, 260)];
        [self addChild:lblNoOfJumpsFromHurdles z:2];
        
        [CCMenuItemFont setFontSize:20];
        
        CCMenuItem *itemBack = [CCMenuItemImage itemWithNormalImage:@"btn-back.png" selectedImage:@"btn-back.png" block:^(id sender) {
            
            [[CCDirector sharedDirector] resume];
            
            CCScene *scene = [CCTransitionFade transitionWithDuration:1.0 scene:[MenuScene scene]];
            [[CCDirector sharedDirector] replaceScene:scene];
            
        }];
        
        CCMenuItemImage *ItemPauseOn = [CCMenuItemImage itemWithNormalImage:@"btn-play.png" selectedImage:@"btn-play.png"];
        CCMenuItemImage *ItemPauseOff = [CCMenuItemImage itemWithNormalImage:@"btn-pause.png" selectedImage:@"btn-pause.png"];
        
        CCMenuItemToggle *itemTogglePauseMenu = [CCMenuItemToggle itemWithTarget:self selector:@selector(pauseMenu:) items:ItemPauseOff, ItemPauseOn, nil];
        [itemTogglePauseMenu setSelectedIndex:0];
        
//        CCMenuItem *itemPause = [CCMenuItemImage itemWithNormalImage:@"btn-pause.png" selectedImage:@"btn-pause.png" block:^(id sender) {
//            
//            [mainMenu setEnabled:NO];
//            [[CCDirector sharedDirector] pause];
//            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isSoundOn"]) {
//                
//                [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
//            }
//            [CCMenuItemFont setFontSize:60];
//            
//            CCMenuItem *itemResume = [CCMenuItemFont itemWithString:@"Resume" block:^(id sender) {
//                
//                [[CCDirector sharedDirector] resume];
//                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isSoundOn"]) {
//                    
//                    [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
//                }
//                [mainMenu setEnabled:YES];
//                [[self getChildByTag:20202] removeFromParentAndCleanup:YES];
//            }];
//            
//            CCMenu *menuResume = [CCMenu menuWithItems:itemResume, nil];
//            [menuResume setPosition:ccp(size.width/2, size.height/2)];
//            [menuResume setTag:20202];
//            [self addChild:menuResume z:400];
//            
//        }];
        
        mainMenu = [CCMenu menuWithItems:itemBack, itemTogglePauseMenu, nil];
        [mainMenu alignItemsHorizontallyWithPadding:-20];
        [mainMenu setPosition:ccp(size.width - 50, 295)];
        
        [self addChild:mainMenu z:5];
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"nameDogi"] length]) {
            
            CCLabelTTF *lblName = [CCLabelTTF labelWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"nameDogi"] fontName:@"Helvetica-Bold" fontSize:15];
            [lblName setPosition:ccp(70, 240)];
            [self addChild:lblName z:2];
        }
        
        //[self renderHorizonSprite];
        
        [self scheduleOnce:@selector(startAllAnimations) delay:1.0];
        
        self.isTouchEnabled = YES;
        
    }
    return self;
}

- (void)pauseMenu:(id)sender {
    
    CCMenuItemToggle *toggle = (CCMenuItemToggle *)sender;
    switch ([toggle selectedIndex]) {
        case 0: {
            [[CCDirector sharedDirector] resume];
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isSoundOn"]) {
                
                [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
            }
        }
            break;
        case 1: {
            [[CCDirector sharedDirector] pause];
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isSoundOn"]) {
                
                [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
            }
        }
            break;
            
        default:
            break;
    }
    
}

- (void)startAllAnimations {
    
    [self renderHorizonSprite];
    
    CGSize size = [[CCDirector sharedDirector] winSize];

    
    greenSprite = [CCSprite spriteWithFile:@"green-background.png"];
    greenSprite.position = ccp(568-190, 50);
    [self addChild:greenSprite z:3];

    if (size.width > 480) {
        fenceFrontSprite = [CCSprite spriteWithFile:@"front-fence10-568h.png"];
        fencebackSprite = [CCSprite spriteWithFile:@"back-fence10-568h.png"];
    }
    else {
        fenceFrontSprite = [CCSprite spriteWithFile:@"front-fence10.png"];
        fencebackSprite = [CCSprite spriteWithFile:@"back-fence10.png"];
    }
    fenceFrontSprite.position = ccp(size.width, 20);
    fencebackSprite.position = ccp(size.width, 150);
    
    [self addChild:fenceFrontSprite z:100 tag:1111];
    [self addChild:fencebackSprite z:4 tag:1110];

    
    [self renderGreenrySprite];
    
    [self renderAnimation];
    
    [self renderFences];
    
    
    if (size.width > 480) {
        peopleSprite = [CCSprite spriteWithFile:@"audience-bar-568h.png"];
    }
    else {
        peopleSprite = [CCSprite spriteWithFile:@"audience-bar.png"];
    }
    [self addChild:peopleSprite z:1];
    peopleSprite.position = ccp(size.width, 160);//447...33
    
    [_burder startProgress];
    
    [self schedule:@selector(renderPeople:) interval:0.1];
    
    [self schedule:@selector(renderCloud:) interval:20.0];
    
    [self scheduleOnce:@selector(startBonusLayer) delay:6.0];
    
    [self schedule:@selector(setDuration) interval:5];
    
    //[_coin schedule:@selector(spinCoin) interval:0.15];
    [self schedule:@selector(setJumpDuration:) interval:10.0];
    
    [self schedule:@selector(giftBoxBonusPack:) interval:40.0];
    
    [self performSelector:@selector(startObstaclesMove) withObject:nil afterDelay:6.0];
    

}

- (void)startBonusLayer {
    
    _randNumOfBonus = arc4random()%10 + 10;
    _bonusPattern = arc4random()%6 + 1;
    
    float rand = arc4random()%4 + 2;
    
    if (_bonusPattern > 3) {
        _randNumOfBonus = 6;
        bunchHeightNum = (int)(arc4random()%3);
        rand = 1.0;
    }
    if (isJetPackOn) {
        _bonusPattern = 6;
        _randNumOfBonus = 6;
        bunchHeightNum = (int)(arc4random()%3);
        rand = 1.0;
    }
    
    [self schedule:@selector(bonusLayerMove) interval:rand/10];
    
}

- (void)bonusLayerMove {
    
    bonusNum ++;
    
    switch (_bonusPattern) {
        case 1: {
            
            int i = bonusNum%4;
            
            i = (i == 1) ? 20 : ((i == 3) ? -20 : 1);
            
            if (i == 1) {
                i = 20;
            }
            else if (i == 3) {
                i = -20;
            }
            
            [self addBonusSpriteWithPositionY:180+i];
            [self addBonusSpriteWithPositionY:250+i];
            
        }
            break;
        case 2: //{
//            int i = bonusNum%2 ? 30 : 1;
//            
//            [self addBonusSpriteWithPositionY:160+i];
//            
//        }
//            break;
        case 3: {
            
            
            [self addBonusSpriteWithPositionY:150];
            [self addBonusSpriteWithPositionY:200];
            [self addBonusSpriteWithPositionY:250];

        }
            break;
        case 4: {
            //int i = bonusNum%2 ? 30 : 1;
            int i = bunchHeightNum*50;
            
            [self addBonusSpriteWithPositionY:200+i];
            [self addBonusSpriteWithPositionY:170+i];
            [self addBonusSpriteWithPositionY:140+i];
            [self addBonusSpriteWithPositionY:110+i];
            [self addBonusSpriteWithPositionY:80+i];
            
        }
            break;
        case 5: {
            int i = bunchHeightNum*50;
            int j = bonusNum%2 ? 15 : 1;
            
            [self addBonusSpriteWithPositionY:200+i+j];
            [self addBonusSpriteWithPositionY:170+i+j];
            [self addBonusSpriteWithPositionY:140+i+j];
            [self addBonusSpriteWithPositionY:110+i+j];
            [self addBonusSpriteWithPositionY:80+i+j];
            
        }
            break;
        case 6: {
            
            int j = (bonusNum-1)*2 + 1;
            
            if (bonusNum > 3) {
                j -= (bonusNum-4)*4 + 2;
            }
            
            int k = (j-1)*10;
            int m = bunchHeightNum*50;

            for (int i=0; i<j; i++) {
                if (isJetPackOn) {
                    [self addBonusSpriteWithPositionY:200+50+k-m];
                }
                else {
                    [self addBonusSpriteWithPositionY:200+k-m];
                }
                k-= 20;
            }
        }
            break;
        default: {
            
            int i = bonusNum%3;
            
            if (i == 1) {
                i = 30;
            }
            else if (i == 2) {
                i = 60;
            }
            
            [self addBonusSpriteWithPositionY:110+i];
            [self addBonusSpriteWithPositionY:160+i];
        }
            break;
    }
    
    if (bonusNum == _randNumOfBonus) {
        [self setBonusSelector];
    }
    
}

- (void)setBonusSelector {
    
    [self unschedule:@selector(bonusLayerMove)];
    bonusNum = 0;
    float delay = 0.5;
    if (!isJetPackOn) {
        delay = arc4random()%5 + 2;
    }
    [self scheduleOnce:@selector(startBonusLayer) delay:delay];

}

- (void)addBonusSpriteWithPositionY:(short )pointY {
    
    if (isGameOver) {
        return;
    }
    
    CCSprite *bonusSprite = [GameManager getRandomBonusSprite];
    bonusSprite.position = ccp(568, pointY);
    [self addChild:bonusSprite z:302];
    bonusSprite.scale = 0.6 ;
    [arrBonus addObject:bonusSprite];
    
    //bonusSpriteMoveDuration = 3;
    
    id actionMove = [CCMoveTo actionWithDuration:bonusSpriteMoveDuration position:ccp(-10, pointY)];
    id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(bonusMoveComplete:)];
    
    [bonusSprite runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
}

- (void)bonusMoveComplete:(CCSprite *)sprite {
    
    if ([arrBonus count]) {
        [sprite removeFromParentAndCleanup:YES];
        [arrBonus removeObject:sprite];
//        [arrBonus[0] removeFromParentAndCleanup:YES];
//        [arrBonus removeObjectAtIndex:0];
    }
}

- (void)startObstaclesMove {
    
    [self schedule:@selector(checkCollision)];

    [self schedule:@selector(randomObstacleMove) interval:2.5];
}

- (void)randomObstacleMove {
    
    //int rand = arc4random()%2+1;
    NSTimeInterval rand = arc4random()%2+obstacleSpriteMoveDuration;
    
    [self performSelector:@selector(obstaclesMove) withObject:nil afterDelay:rand];
}

- (void)obstaclesMove {
    
    if (isGameOver) {
        return;
    }
    
    CCSprite *obstacleSprite = [GameManager getRandomObstacleSprite];
    
    obstacleSprite.position = ccp(568, 70);
    
    [self addChild:obstacleSprite z:8];
    //obstacleSprite.tag = 100;
    [arrObstacle addObject:obstacleSprite];
    
    id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(obstacleMoveComplete:)];
    
    [obstacleSprite runAction:[CCSequence actions:[CCMoveTo actionWithDuration:obstacleSpriteMoveDuration position:ccp(0, 80)], actionMoveDone, nil]];
}

- (void)obstacleMoveComplete:(id)sender {
    
    CCSprite *sprite = (CCSprite *)sender;
    if (sprite.tag == 100 || sprite.tag == 101) {
        
        if (sprite.tag == 100) {
            int hurdleCounts = [[lblNoOfJumpsFromHurdles string] intValue];
            hurdleCounts += 1;
            [lblNoOfJumpsFromHurdles setString:[NSString stringWithFormat:@"%d", hurdleCounts]];
            _burder.progressBar.percentage += 1;
        }
        
        [arrObstacle removeObject:sprite];
    }
    if (sprite.tag == 1200) {
        [self scheduleOnce:@selector(shieldPack) delay:2.0f];
    }
	[self removeChild:sprite cleanup: YES];
    
}

- (void)removeAllBonusesAndObstacles {
    
    if ([self.children containsObject:_spriteObstacleNotRemove]) {
        [_spriteObstacleNotRemove stopAllActions];
    }
    
    [self unschedule:@selector(bonusLayerMove)];
    [self unschedule:@selector(startBonusLayer)];

    [self unschedule:@selector(obstaclesMove)];
    [self unschedule:@selector(startObstaclesMove)];
    [self unschedule:@selector(randomObstacleMove)];

    for (CCSprite *sp in arrBonus) {
        [sp removeFromParentAndCleanup:YES];
    }
    for (CCSprite *sp in arrObstacle) {
        if (_spriteObstacleNotRemove != sp) {
            
            [sp removeFromParentAndCleanup:YES];
        }
    }
    [arrBonus removeAllObjects];
    [arrObstacle removeAllObjects];
}

- (void)setDuration {
    
    if (bonusSpriteMoveDuration > 1.0) {
        
        bonusSpriteMoveDuration -= 0.5;
    }
    if (obstacleSpriteMoveDuration > 1.0) {
        
        obstacleSpriteMoveDuration -= 0.1;
    }
}

- (void)checkCollision {
    
    if ([arrBonus count]) {
        
        CCSprite *spriteToRemove = nil;
        
        CGRect rect1 = dogJumpSprite.boundingBox;
        CGRect rect2 = dogSprite.boundingBox;
        
//        CGRect rect1 = CGRectMake(235, dogJumpSprite.position.y - 35, 50, 50);
//        CGRect rect2 = CGRectMake(235, dogSprite.position.y - 35, 50, 50);
        
        for (CCSprite *sprite in arrBonus) {
            
            CGRect rect = (dogJetPack.visible)? dogJetPack.boundingBox: (dogSprite.visible)? rect2 : rect1;
            
            if (CGRectIntersectsRect(rect, sprite.boundingBox)) {
                
                spriteToRemove = sprite;
                
                CCSprite *spp;
                
                switch (sprite.tag) {
                    case 204: // bone
                        spp = [CCSprite spriteWithFile:@"bone.png"];
                        [self moveBonusHeaderSpriteWithCPosition:sprite.position andFilePosition:ccp(180, 300) andSprite:spp];
                        //[self moveBonusHeaderSpriteWithCPosition:sprite.position andFilePosition:ccp(180, 300) andFileName:@"bone.png"];
                        break;
                    case 200: // fish
                        spp = [CCSprite spriteWithFile:@"fish.png"];
                        [self moveBonusHeaderSpriteWithCPosition:sprite.position andFilePosition:ccp(240, 300) andSprite:spp];
                        //[self moveBonusHeaderSpriteWithCPosition:sprite.position andFilePosition:ccp(240, 300) andFileName:@"fish.png"];
                        break;
                    case 201: // leg-piece
                        spp = [CCSprite spriteWithFile:@"leg-piece.png"];
                        [self moveBonusHeaderSpriteWithCPosition:sprite.position andFilePosition:ccp(300, 300) andSprite:spp];
                        //[self moveBonusHeaderSpriteWithCPosition:sprite.position andFilePosition:ccp(300, 300) andFileName:@"leg-piece.png"];
                        break;
                    case 203: // pizza
                        spp = [CCSprite spriteWithFile:@"pizza.png"];
                        [self moveBonusHeaderSpriteWithCPosition:sprite.position andFilePosition:ccp(360, 300) andSprite:spp];
                        //[self moveBonusHeaderSpriteWithCPosition:sprite.position andFilePosition:ccp(360, 300) andFileName:@"pizza.png"];
                        break;
                    case 1010: {
                        isJetPackOn = YES;
                        [dogJetPack setVisible:YES];
                        [dogJetPack setPosition:dogJumpSprite.position];
                        [dogSprite setVisible:NO];
                        [dogJumpSprite setVisible:NO];
                        bonusSpriteMoveDuration += 0.2;
                        obstacleSpriteMoveDuration += 0.2;
                        //bonusSpriteMoveDuration = 2.5 - ((float )numOfTimesRocket/5.0);
                        //obstacleSpriteMoveDuration = 1.8 - ((float )numOfTimesRocket/5.0);
                        //bonusNum = (_randNumOfBonus-5);
                        [self setBonusSelector];
                        CCMoveTo *move = [CCMoveTo actionWithDuration:0.5 position:ccp(180, 250)];
                        [dogJetPack runAction:move];
                        
                        [self scheduleOnce:@selector(prepareToRemoveJetPack) delay:10.0];
                    }
                        break;
                    case 1011: {
                        isShieldOn = YES;
                        [shieldSprite1 setVisible:YES];
                        [shieldSprite2 setVisible:YES];
                        
                        [self scheduleOnce:@selector(prepareToRemoveShieldPack) delay:10.0];
                    }
                        break;
                    default: {
                        CCSprite *posball=[CCSprite spriteWithFile:@"water-splash.png"];
                        posball.position=sprite.position;
                        //[posball runAction:[CCMoveTo actionWithDuration:0.3 position:ccp(boneSprite.position.x,boneSprite.position.y+1.5*boneSprite.contentSize.height)]];
                        [posball runAction:[CCFadeOut actionWithDuration:1.5]];
                        [self performSelector:@selector(removeWhiteThing:) withObject:posball afterDelay:1];
                        [self addChild:posball z:20];
                    }
                        break;
                }
                
                [arrToRemoveObj addObject:sprite];
            }
        }
        
        if ([arrToRemoveObj count]) {
            
//            if (_burder.progressBar.percentage < 100.0) {
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isSoundOn"]) {
                    [[SimpleAudioEngine sharedEngine] playEffect:@"Get-energy.mp3"];
                }

                if (spriteToRemove.tag == 200) {
                    _burder.progressBar.percentage += 5;
                }
                else {
                    _burder.progressBar.percentage += 1;
                }
//            }
            
            for (CCSprite *cp in arrToRemoveObj) {
                
                switch (cp.tag) {
                    case 204: // bone
                        numOfBones += 1;
                        [bHBone.lblCoin setString:[NSString stringWithFormat:@"%d", numOfBones]];
                        break;
                    case 200: // fish
                        numOfFishes += 1;
                        [bHFish.lblCoin setString:[NSString stringWithFormat:@"%d", numOfFishes]];
                        break;
                    case 201: // leg-piece
                        numOfLegPieces += 1;
                        [bHLegPiece.lblCoin setString:[NSString stringWithFormat:@"%d", numOfLegPieces]];
                        break;
                    case 203: // pizza
                        numOfPizzas += 1;
                        [bHPizza.lblCoin setString:[NSString stringWithFormat:@"%d", numOfPizzas]];
                        break;
                        
                    default:
                        break;
                }
                
                [cp stopAllActions];
                [cp removeFromParentAndCleanup:YES];
                [arrBonus removeObject:cp];
            }
            [arrToRemoveObj removeAllObjects];
            
        }
    }
    
    if ([arrObstacle count]) {
        
        for (CCSprite *sprite in arrObstacle) {
            
            if (sprite.tag == 100 && [dogSprite visible]) {
                
                if (CGRectIntersectsRect(dogSprite.boundingBox, sprite.boundingBox)) {
                    
                    CGRect intersectRect = CGRectIntersection(dogSprite.boundingBox, sprite.boundingBox);
                    CGPoint midPoint = CGPointMake(intersectRect.origin.x+intersectRect.size.width/2,
                                                   intersectRect.origin.y+intersectRect.size.height/2);
                    
                    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isSoundOn"]) {
                        [[SimpleAudioEngine sharedEngine] playEffect:@"Hit.mp3"];
                    }
                    
                    if (isShieldOn) {
                        CCSprite *sp = [CCSprite spriteWithFile:@"boom.png"];
                        [self moveBonusHeaderSpriteWithCPosition:sprite.position andFilePosition:ccp(568, 0) andSprite:sp];
                        
                    }
                    else {
                        
                        [dogHeadStarsSprite setVisible:YES];
                        [self scheduleOnce:@selector(setStarsHidden) delay:1.5];
                        dogSprite.tag = 400;
                        
                        CCSprite *posball=[CCSprite spriteWithFile:@"boom.png"];
                        
                        if (isJetPackOn) {
                            [self schedule:@selector(dogHideAndShow) interval:0.1];
                        }
                        posball.position=midPoint;
                        [posball runAction:[CCFadeOut actionWithDuration:1.5]];
                        [self performSelector:@selector(removeWhiteThing:) withObject:posball afterDelay:1];
                        [self addChild:posball z:20];
                        
                        sprite.tag = 101;
                        _burder.progressBar.percentage -= 10;
                    }
                    _spriteObstacleNotRemove = sprite;
                }
            }
            else if (sprite.tag == 1200 && [dogSprite visible]) {
                
                if (CGRectContainsPoint(sprite.boundingBox, dogSprite.position)) {
                    
                    //CGRect intersectRect = CGRectIntersection(dogSprite.boundingBox, sprite.boundingBox);

                    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isSoundOn"]) {
                        [[SimpleAudioEngine sharedEngine] playEffect:@"Hit.mp3"];
                    }
                    
                    isGameOver = YES;
                    
                    [self unscheduleAllSelectors];
                    [_burder unschedule:@selector(walkEnergyLoosing)];
                    [self unschedule:@selector(randomObstacleMove)];
                    [self setIsTouchEnabled:NO];
                    [dogSprite stopAllActions];
                    [dogShadow setVisible:NO];
                    
                    [dogSprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"spriteFrame2"]];
                    
                    [dogSprite setPosition:ccp(dogSprite.position.x+20, dogSprite.position.y)];
                    
                    CGPoint p = ccp(sprite.position.x, sprite.position.y-sprite.contentSize.height/2);
                    
                    if ([shieldSprite1 visible]) {
                        [shieldSprite1 runAction:[CCFadeOut actionWithDuration:0.5]];
                    }
                    
                    [dogSprite runAction:[CCMoveTo actionWithDuration:1.0f position:p]];
                    [dogSprite runAction:[CCScaleTo actionWithDuration:1.0f scale:0.01f]];
                    [dogSprite runAction:[CCRepeatForever actionWithAction:[CCRotateBy actionWithDuration:1.0f angle:200.0f]]];
                    [self scheduleOnce:@selector(gameOverMenu) delay:1.5f];
                    
                    _spriteObstacleNotRemove = sprite;
                    [sprite stopAllActions];
                    [self removeAllBonusesAndObstacles];
                    [self gameOver];
                    [self unschedule:@selector(checkCollision)];
                    
                }
            }
        }
    }
    
    if (_burder.progressBar.percentage == 0.0) {
        
        isGameOver = YES;
        
        [dogSprite stopAllActions];
        [dogSprite setVisible:YES];
        [dogShadow stopAllActions];
        [dogShadow setVisible:NO];
        [dogJumpSprite stopAllActions];
        [dogJumpSprite setVisible:NO];
        [dogJumpShadow stopAllActions];
        [dogJumpShadow setVisible:NO];
        [dogHeadStarsSprite setVisible:NO];
        [dogJetPack setVisible:NO];
//        [wingSprite setVisible:YES];
        [self setIsTouchEnabled:NO];
        [self unschedule:@selector(checkCollision)];

        [dogSprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"spriteFrame2"]];
        
        
        [self scheduleOnce:@selector(dogFly) delay:1.0f];
        [self removeAllBonusesAndObstacles];
        //[self dogBlast];
        [self gameOver];
    }
}

- (void)dogFly {
    
    [wingSprite setVisible:YES];
    
    [dogSprite runAction:[CCMoveTo actionWithDuration:5.0f position:ccp(668, 400)]];
    [dogSprite runAction:[CCFadeOut actionWithDuration:5.0f]];
    [dogSprite runAction:[CCScaleTo actionWithDuration:5.0f scale:0.2]];

    [self scheduleOnce:@selector(gameOverMenu) delay:5.0f];
    //[self gameOver];
}

- (void)setStarsHidden {
    [dogHeadStarsSprite setVisible:NO];
}

- (void)removeWhiteThing:(id)posBall {
    [posBall removeFromParentAndCleanup:YES];
}

- (void)moveBonusHeaderSpriteWithCPosition:(CGPoint )cPoint andFilePosition:(CGPoint )fPoint andSprite:(CCSprite *)posball {
    
    //CCSprite *posball=[CCSprite spriteWithFile:fileName];
    
    [posball setPosition:cPoint];
    [posball setScale:0.5];
    [self addChild:posball];
    
    id move = [CCMoveTo actionWithDuration:0.5 position:fPoint];
    id scale = [CCScaleTo actionWithDuration:0.5 scale:0.4];
    id moveEnd = [CCCallFuncN actionWithTarget:self selector:@selector(obstacleMoveComplete:)];
    [posball runAction:[CCSequence actions:move, scale, moveEnd, nil]];

}

- (void)dogHideAndShow {
    
    if ([dogSprite visible]) {
        [dogSprite setVisible:NO];
    }
    else {
        [dogSprite setVisible:YES];
    }
    
    dogSprite.tag += 1;
    
    if (dogSprite.tag == 410) {
        [self unschedule:@selector(dogHideAndShow)];
    }
}

- (void)renderAnimation {
    
    CCSpriteFrame *spriteFrame1 = [self dogFrame:1 isJumpDogi:NO];
    CCSpriteFrame *spriteFrame2 = [self dogFrame:2 isJumpDogi:NO];
    CCSpriteFrame *spriteFrame3 = [self dogFrame:3 isJumpDogi:NO];
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFrame:spriteFrame2 name:@"spriteFrame2"];
    
    CCSprite *spriteTotalJumps = [CCSprite spriteWithSpriteFrame:spriteFrame3];
    [spriteTotalJumps setPosition:ccp(160, 270)];
    [spriteTotalJumps setRotation:-30];
    [spriteTotalJumps setScale:0.2];
    [self addChild:spriteTotalJumps z:3];
    
    dogJetPack = [CCSprite spriteWithSpriteFrame:spriteFrame3];
    [dogJetPack setPosition:ccp(180, 250)];
    [dogJetPack setScale:0.6];
    [self addChild:dogJetPack z:10];
    
    CCSprite *jetSprite = [CCSprite spriteWithFile:@"jet-pack-fire.png"];
    [jetSprite setPosition:ccp(jetSprite.contentSize.width/2, jetSprite.contentSize.height/2)];
    [dogJetPack addChild:jetSprite z:10];
    //[self addRocketFire];
    [self scheduleOnce:@selector(addRocketFire) delay:10.0f];
    
//    
//    CCParticleFire *particleEffect = [CCParticleFire node];
//    particleEffect set
    
    [dogJetPack setVisible:NO];
    
    dogSprite = [CCSprite spriteWithSpriteFrame:spriteFrame1];
    dogSprite.position = ccp(180, 80);
    dogSprite.scale = 0.6;
    [self addChild:dogSprite z:10];
    
    shieldSprite1 = [CCSprite spriteWithFile:@"dog-shield.png"];
    [shieldSprite1 setPosition:ccp(dogSprite.contentSize.width/2, dogSprite.contentSize.height/2)];
    [dogSprite addChild:shieldSprite1 z:10];
    [shieldSprite1 setVisible:NO];
    
    dogShadow = [CCSprite spriteWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"shadow-22.png"]];
    dogShadow.position = ccp(180, 55);
    dogShadow.scale = 1.1;
    [self addChild:dogShadow z:9];
    
    CCAnimation *aniDogRun= [CCAnimation animation];
    CCAnimation *aniDogShad = [CCAnimation animation];
    
    aniDogRun.delayPerUnit = 0.1;
    aniDogShad.delayPerUnit = 0.15;
    
    [aniDogRun addSpriteFrame:spriteFrame1];
    [aniDogRun addSpriteFrame:spriteFrame2];
    [aniDogRun addSpriteFrame:spriteFrame3];
    
    [aniDogShad addSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"shadow-11.png"]];
    [aniDogShad addSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"shadow-22.png"]];
    
    [dogSprite runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:aniDogRun]]];
    [dogShadow runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:aniDogShad]]];
    
    
    dogJumpSprite = [CCSprite spriteWithSpriteFrame:[self dogFrame:3 isJumpDogi:YES]];
    [self addChild:dogJumpSprite z:10];
    dogJumpSprite.scale = 0.6;
    [dogJumpSprite setVisible:NO];
    
    shieldSprite2 = [CCSprite spriteWithFile:@"dog-shield.png"];
    [shieldSprite2 setPosition:ccp(dogSprite.contentSize.width/2, dogSprite.contentSize.height/2)];
    [dogJumpSprite addChild:shieldSprite2 z:10];
    [shieldSprite2 setVisible:NO];

    
    dogJumpShadow = [CCSprite spriteWithSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"shadow-22.png"]];
    dogJumpShadow.position = ccp(180, 55);
    dogJumpShadow.scale = 1.1;
    [self addChild:dogJumpShadow z:9];
    [dogJumpShadow setVisible:NO];
    
    // head stars
    
    dogHeadStarsSprite = [CCSprite spriteWithFile:@"head-stars-1.png"];
    [dogHeadStarsSprite setPosition:ccp(180, 80)];
    [dogHeadStarsSprite setScale:0.5];
    [self addChild:dogHeadStarsSprite z:20];
    [dogHeadStarsSprite setVisible:NO];
    
    CGRect rr = CGRectMake(0, 0, 200, 100);
    
    CCSpriteFrame *spriteStars1 = [CCSpriteFrame frameWithTexture:[[CCSprite spriteWithFile:@"head-stars-1.png"] texture] rect:rr];
    CCSpriteFrame *spriteStars2 = [CCSpriteFrame frameWithTexture:[[CCSprite spriteWithFile:@"head-stars-2.png"] texture] rect:rr];
    CCSpriteFrame *spriteStars3 = [CCSpriteFrame frameWithTexture:[[CCSprite spriteWithFile:@"head-stars-3.png"] texture] rect:rr];
    
    CCAnimation *aniHeadStars = [CCAnimation animation];
    [aniHeadStars setDelayPerUnit:0.3];
    
    [aniHeadStars addSpriteFrame:spriteStars1];
    [aniHeadStars addSpriteFrame:spriteStars2];
    [aniHeadStars addSpriteFrame:spriteStars3];
    
    [dogHeadStarsSprite runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:aniHeadStars]]];
    
    // wings
    
    wingSprite = [CCSprite spriteWithFile:@"wings-animation-1.png"];
    //[wingSprite setPosition:ccp(180, 80)];
    [wingSprite setPosition:ccp(dogSprite.contentSize.width/2, dogSprite.contentSize.height/2)];
    //[wingSprite setScale:0.6];
    [dogSprite addChild:wingSprite z:30];
    [wingSprite setVisible:NO];

    CCSpriteFrame *spriteWing1 = [CCSpriteFrame frameWithTexture:[[CCSprite spriteWithFile:@"wings-animation-1.png"] texture] rect:rr];
    CCSpriteFrame *spriteWing2 = [CCSpriteFrame frameWithTexture:[[CCSprite spriteWithFile:@"wings-animation-2.png"] texture] rect:rr];
    CCSpriteFrame *spriteWing3 = [CCSpriteFrame frameWithTexture:[[CCSprite spriteWithFile:@"wings-animation-3.png"] texture] rect:rr];
    
    CCAnimation *aniWings = [CCAnimation animation];
    [aniWings setDelayPerUnit:0.1];
    
    [aniWings addSpriteFrame:spriteWing1];
    [aniWings addSpriteFrame:spriteWing2];
    [aniWings addSpriteFrame:spriteWing3];
    
    [wingSprite runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:aniWings]]];

}

- (void)jumpDogiWithHeight:(int)height andDuration:(float )duration {
    
    [self unschedule:@selector(dogHideAndShow)];
    
    _burder.progressBar.percentage -= 2;
    
    [dogJumpSprite setVisible:YES];
    dogJumpSprite.rotation = -30;
    dogJumpSprite.position = ccp(180,80);
    [self setIsTouchEnabled:NO];
    
    [dogSprite setVisible:NO];
    [dogShadow setVisible:NO];
    [dogHeadStarsSprite setVisible:NO];
    
    id rotateAction = [CCRotateTo actionWithDuration:duration angle:2];
    
    id jump = [CCJumpBy actionWithDuration:duration position:ccp(0,0) height:height jumps:1];
    id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(dogJumpComplete:)];

    id jumpDoneAction = [CCSequence actions:jump, actionMoveDone, nil];
    
    [dogJumpSprite runAction:rotateAction];
    [dogJumpSprite runAction:jumpDoneAction];
    
    [dogJumpShadow setVisible:YES];
    
    id actionMoveUp = [CCCallFuncN actionWithTarget:self selector:@selector(dogJumpUpDone:)];
    id action = [CCSequence actions:[CCScaleTo actionWithDuration:duration/2 scale:0.5], actionMoveUp, nil];
    
    [dogJumpShadow runAction:action];
    
}

- (void)dogJumpUpDone:(id)sender {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isSoundOn"]) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"Jump.mp3"];
    }

    [dogJumpShadow runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
}

- (void)dogJumpComplete:(id)sender {
    
    [self setIsTouchEnabled:YES];
    
    if (!isJetPackOn) {
        [dogSprite setVisible:YES];
        [dogShadow setVisible:YES];
    }
    [dogJumpSprite setVisible:NO];
    [dogJumpShadow setVisible:NO];
    
    
//    [self.camera setCenterX:0 centerY:0 centerZ:0];
//    [self.camera setEyeX:0 eyeY:0 eyeZ:0];
    
}

- (void)renderPeople:(ccTime)dt {
    
    if (peopleSprite.position.x < 5) {
        peopleSprite.position = ccp(peopleSprite.contentSize.width/2, 160);
    }
    else {
        CGPoint pos;
        pos = peopleSprite.position;
        pos.x -= 5;
        peopleSprite.position = pos;
    }
    
}

- (void)renderGreenrySprite {
    
//    CCSprite *greenSprite = [CCSprite spriteWithFile:@"green-background.png"];
//    greenSprite.position = ccp(568-190, 50);
//    [self addChild:greenSprite z:3];
    
//    ccTime actualDuration = 2;
//    
//    id actionMoveTo = [CCMoveTo actionWithDuration:actualDuration position:ccp(0, 50)];
//    id initPosition = [CCPlace actionWithPosition:ccp(568-190, 50)];
//    id rptFrvr1 = [CCRepeatForever actionWithAction:[CCSequence actions:actionMoveTo, initPosition, nil]];
//    [greenSprite runAction:rptFrvr1];
    
    
    id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(greenSpriteMoveDone:)];
    
    [greenSprite runAction:[CCSequence actions:[CCMoveTo actionWithDuration:obstacleSpriteMoveDuration-0.6 position:ccp(90, 50)], actionMoveDone, nil]];
    
}

- (void)greenSpriteMoveDone:(id)sender {
    
    greenSprite.position = ccp(568-190, 50);
    [self renderGreenrySprite];
}

- (void)renderHorizonSprite {
    
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    CCSprite *character;
    
    if (size.width > 480) {
        character = [CCSprite spriteWithFile:@"building-bar-568h.png"];
    }
    else {
        character = [CCSprite spriteWithFile:@"building-bar.png"];
    }
    character.position = ccp(size.width, 200);
    [self addChild:character z:1 tag:2345];
    
    ccTime actualDuration = 25;
    
    id actionMoveTo = [CCMoveTo actionWithDuration:actualDuration position:ccp(0, 200)];
    id initPosition = [CCPlace actionWithPosition:ccp(size.width, 200)];
    id rptFrvr1 = [CCRepeatForever actionWithAction:[CCSequence actions:actionMoveTo, initPosition, nil]];
    [character runAction:rptFrvr1];
    
}

- (void)renderFences {
    

    [self frontFenceMove];
    [self backFenceMove];
    
/*    CCSprite *fenceFrontSprite = [CCSprite spriteWithFile:@"front-fence1.png"];
    CCSprite *fencebackSprite = [CCSprite spriteWithFile:@"back-fence1.png"];
    fenceFrontSprite.position = ccp(284, 20);
    fencebackSprite.position = ccp(284, 150);
    
    [self addChild:fenceFrontSprite z:100 tag:1111];
    [self addChild:fencebackSprite z:4 tag:1110];
    
    CCAnimation *animationFrontFence= [CCAnimation animation];
    CCAnimation *animationbackFence= [CCAnimation animation];
    
    animationFrontFence.delayPerUnit = 0.15;
    animationbackFence.delayPerUnit = 0.15;
    for(int i = 1; i <= 2; i++) {
        [animationFrontFence addSpriteFrameWithFilename:[NSString  stringWithFormat:@"front-fence%d.png", i]];
        [animationbackFence addSpriteFrameWithFilename:[NSString  stringWithFormat:@"back-fence%d.png", i]];
    }
    id animationFrontAction = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:animationFrontFence]];
    id animationBackAction = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:animationbackFence]];
    [fenceFrontSprite runAction:animationFrontAction];
    [fencebackSprite runAction:animationBackAction];*/
    
}

- (void)frontFenceMove {
    
//    id actionMoveTo1 = [CCMoveTo actionWithDuration:obstacleSpriteMoveDuration position:ccp(0, 20)];
//    id initPosition1 = [CCPlace actionWithPosition:ccp(568, 20)];
//    id rptFrvr1 = [CCRepeatForever actionWithAction:[CCSequence actions:actionMoveTo1, initPosition1, nil]];
//    [fenceFrontSprite runAction:rptFrvr1];

    id actionMove = [CCMoveTo actionWithDuration:obstacleSpriteMoveDuration position:ccp(0, 20)];
    id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(frontFenceMoveDone)];
    
    [fenceFrontSprite runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];

}

- (void)backFenceMove {
    
//    id actionMoveTo2 = [CCMoveTo actionWithDuration:obstacleSpriteMoveDuration*0.2 position:ccp(0, 150)];
//    id initPosition2 = [CCPlace actionWithPosition:ccp(568, 150)];
//    id rptFrvr2 = [CCRepeatForever actionWithAction:[CCSequence actions:actionMoveTo2, initPosition2, nil]];
//    [fencebackSprite runAction:rptFrvr2];
    
    id actionMove = [CCMoveTo actionWithDuration:obstacleSpriteMoveDuration*2.0 position:ccp(0, 150)];
    id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(backFenceMoveDone)];
    
    [fencebackSprite runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
    
}

- (void)frontFenceMoveDone {
    
    CGSize size = [[CCDirector sharedDirector] winSize];

    [fenceFrontSprite setPosition:ccp(size.width, 20)];
    [self frontFenceMove];
}

- (void)backFenceMoveDone {
    
    CGSize size = [[CCDirector sharedDirector] winSize];

    [fencebackSprite setPosition:ccp(size.width, 150)];
    [self backFenceMove];
}

- (void) renderCloud: (ccTime)dt {
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    int tagNum =  (arc4random()%3+1);
	NSString *strCloud = [NSString stringWithFormat:@"cloude-%i.png",tagNum];
    
    CCSprite *cloud = [CCSprite spriteWithFile:strCloud];
	[self addChild:cloud];
    
	cloud.position = ccp(winSize.width + cloud.contentSize.width/2, 250 + (arc4random()% 4) * 20);
	
	int duration = 50 + arc4random() % 20;
	id actionMove = [CCMoveTo actionWithDuration:duration position:ccp(-cloud.contentSize.width/2, cloud.position.y)];
    
	id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(cloudMoveDone:)];
	[cloud runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
    
}

- (void) cloudMoveDone: (id) sender {
	CCSprite *cloud = (CCSprite *)sender;
	[self removeChild:cloud cleanup: YES];
}


- (void)gameOver {
    
    [shieldSprite1 setVisible:NO];
    [shieldSprite2 setVisible:NO];
    
    CCSprite *spFence1 = (CCSprite *)[self getChildByTag:1111];
    CCSprite *spFence2 = (CCSprite *)[self getChildByTag:1110];
    
    CCSprite *building = (CCSprite *)[self getChildByTag:2345];
    
    [spFence1 stopAllActions];
    [spFence2 stopAllActions];
    [building stopAllActions];
    [greenSprite stopAllActions];
    [dogShadow stopAllActions];
    [self unschedule:@selector(renderPeople:)];
    [self scheduleOnce:@selector(setStarsHidden) delay:0.5];
    
    [mainMenu setVisible:NO];
    
}

- (void)gameOverMenu {
    
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    CCLabelTTF *gameOver = [[CCLabelTTF alloc] initWithString:@"GAME OVER" fontName:@"Helvetica-Bold" fontSize:60];
    gameOver.position = ccp(size.width/2,160);
    gameOver.opacity = 0;
    [self addChild:gameOver z:400];
    [gameOver release];
    
    
    CCSequence *seq1 = [CCSequence actions:
                        [CCCallBlock actionWithBlock:(^{[self unscheduleAllSelectors];})],
                        [CCFadeIn actionWithDuration:1.0],
                        [CCDelayTime actionWithDuration:0.5],
                        [CCFadeOut actionWithDuration:1.0],
                        [CCCallBlock actionWithBlock:(^{
        
        
        
        [CCMenuItemFont setFontSize:60];
        
        CCMenuItem *itemReplay = [CCMenuItemFont itemWithString:@"Play again" block:^(id sender) {
            
            CCScene *scene = [CCTransitionCrossFade transitionWithDuration:1.0 scene:[GamePlayLayer scene]];
            [[CCDirector sharedDirector] replaceScene:scene];
        }];
        CCMenuItem *itemBack = [CCMenuItemFont itemWithString:@"Back" block:^(id sender) {
            
            [[self getChildByTag:10101] removeFromParentAndCleanup:YES];
            [self goMainScreen];
        }];
        CCMenu *menuGameOver = [CCMenu menuWithItems:itemReplay, itemBack, nil];
        [menuGameOver alignItemsVertically];
        menuGameOver.opacity = 0;
        [menuGameOver setPosition:ccp(size.width/2, 160)];
        [menuGameOver setTag:10101];
        [self addChild:menuGameOver z:400];
        [menuGameOver runAction:[CCFadeIn actionWithDuration:0.5]];
        [self scheduleOnce:@selector(goMainScreen) delay:10.0f];
        
    })], nil];
    
    [gameOver runAction:seq1];
    
}

- (void)goMainScreen {
    CCScene *scene = [CCTransitionFade transitionWithDuration:1.0 scene:[MenuScene scene]];
    [[CCDirector sharedDirector] replaceScene:scene];

}

- (void)onEnter {
    
    CCTouchDispatcher *touchDispatcher = [[[CCTouchDispatcher alloc] init] autorelease];
    [touchDispatcher addTargetedDelegate:self priority:0 swallowsTouches:NO];
    
    [super onEnter];
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    
    return YES;
}

- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    ismove = NO;
    
    UITouch *touch = [[event allTouches] anyObject];
    startPoint = [touch locationInView:[[CCDirector sharedDirector] view]];
    
}

- (void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    ismove = YES;
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (ismove) {
        
        ismove = NO;
        
        UITouch *touch = [[event allTouches] anyObject];
        
        endPoint = [touch locationInView:[[CCDirector sharedDirector] view]];
        
        float diff = startPoint.y - endPoint.y;
        
        if (isJetPackOn) {
            if (diff > 0) {
                if (dogJetPack.position.y <= 200.0) {
                    [dogJetPack runAction:[CCMoveTo actionWithDuration:0.3 position:ccp(180, dogJetPack.position.y + 50)]];
                    //[dogJetPack setPosition:ccp(180, dogJetPack.position.y + 50)];
                }
            }
            else {
                if (dogJetPack.position.y >= 200.0) {
                    [dogJetPack runAction:[CCMoveTo actionWithDuration:0.3 position:ccp(180, dogJetPack.position.y - 50)]];
                    //[dogJetPack setPosition:ccp(180, dogJetPack.position.y - 50)];
                }
            }
        }
        else {
            if (diff > 0) {
                if (diff<=100) {
                    [self jumpDogiWithHeight:100 andDuration:1.0-dogJumpDuration];
                }
                else if (diff > 100 && diff < 150) {
                    [self jumpDogiWithHeight:diff andDuration:(diff/100.0)-dogJumpDuration];
                }
                else {
                    [self jumpDogiWithHeight:150 andDuration:1.5-dogJumpDuration];
                }
            }
        }
    }
}

- (void)setJumpDuration:(ccTime)time {
    
    if (dogJumpDuration < 0.5) {
        
        dogJumpDuration += 0.05;
    }
}

- (void)giftBoxBonusPack:(ccTime)time {
    
    if (isShieldOn || isGameOver) {
        return;
    }
    
    int y = arc4random()%100 + 150;
    
    numOfTimesRocket += 1;
    
    CCSprite *giftSprite = [CCSprite spriteWithFile:@"jet-pack-icon.png"];
    giftSprite.position = ccp(568, y);
    giftSprite.tag = 1010;
    [self addChild:giftSprite z:302];
    giftSprite.scale = 0.6 ;
    [arrBonus addObject:giftSprite];
    
    //bonusSpriteMoveDuration = 3;
    
    id actionMove = [CCMoveTo actionWithDuration:3.0 position:ccp(-10, y)];
    id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(bonusMoveComplete:)];
    
    [giftSprite runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];

}

- (void)prepareToRemoveJetPack {
    
    id actionMove = [CCMoveTo actionWithDuration:0.5 position:dogSprite.position];
    id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(removeJetPack)];
    [dogJetPack runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
}

- (void)removeJetPack {
    
    isJetPackOn = NO;
    [dogJetPack setVisible:NO];
    [dogSprite setVisible:YES];
    bonusSpriteMoveDuration = 2.5;
    dogJumpDuration = 0.1;
    
    [self scheduleOnce:@selector(shieldPack) delay:5.0f];
}

- (void)shieldPack {
    
    if (isJetPackOn || isGameOver) {
        return;
    }
    
    int y = arc4random()%100 + 150;
    
    CCSprite *giftSprite = [CCSprite spriteWithFile:@"shield-icon.png"];
    giftSprite.position = ccp(568, y);
    giftSprite.tag = 1011;
    [self addChild:giftSprite z:302];
    giftSprite.scale = 0.6 ;
    [arrBonus addObject:giftSprite];
    
    //bonusSpriteMoveDuration = 3;
    
    id actionMove = [CCMoveTo actionWithDuration:3.0 position:ccp(0, y)];
    id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(bonusMoveComplete:)];
    
    [giftSprite runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];

}

- (void)prepareToRemoveShieldPack {
    
    //[self removeShieldPack];
    
//    id actionFade = [CCFadeOut actionWithDuration:0.5f];
//    id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(removeShieldPack)];
//    [shieldSprite1 runAction:[CCSequence actions:actionFade, actionMoveDone, nil]];
    shieldSprite1.tag = 400;
    [self schedule:@selector(shieldBlink) interval:0.1];
}

- (void)shieldBlink {
    
    if ([shieldSprite1 visible]) {
        [shieldSprite1 setVisible:NO];
        [shieldSprite2 setVisible:NO];
    }
    else {
        [shieldSprite1 setVisible:YES];
        [shieldSprite2 setVisible:YES];
    }
    
    shieldSprite1.tag += 1;
    
    if (shieldSprite1.tag == 410) {
        [self unschedule:@selector(shieldBlink)];
        [self removeShieldPack];
    }
}

- (void)removeShieldPack {
    
    isShieldOn = NO;
    [shieldSprite1 setVisible:NO];
    [shieldSprite2 setVisible:NO];
}

- (void)dogBlast {
    
    CCParticleExplosion* emitter = [CCParticleExplosion node];
    emitter.autoRemoveOnFinish = YES;
    emitter.texture = [dogSprite texture];
    emitter.startSize = 5.0f;
    emitter.endSize = 1.0f;
    emitter.duration = 0.1f;
    emitter.speed = 50.0f;
    emitter.anchorPoint = ccp(0.5f,0.5f);
    emitter.position = dogSprite.position;
    
    [self addChild: emitter z:100];
    [dogSprite setVisible:NO];
    [dogShadow setVisible:NO];
    [dogJumpSprite setVisible:NO];
    [dogJumpShadow setVisible:NO];
    
}

- (void)addRocketFire {
    
    CCParticleSystem *emitter = [CCParticleFire node];
    
    //set the location of the emitter
    //emitter.position = ccp(240, 160);
    emitter.position = ccp(dogJetPack.contentSize.width/2, dogJetPack.contentSize.height/2);
    emitter.autoRemoveOnFinish = YES;
    
    //set size of particle animation
    //emitter.scale = 0.5;
    
    emitter.emitterMode = kCCParticleModeRadius;
    
    emitter.emissionRate = 100;
    emitter.angle = 0;
    //set an Image for the particle
    emitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"jet-fire.png"];//[[CCSprite spriteWithFile:@"jet-fire.png"] texture];
    
    [emitter setLife:0.5f];
    emitter.speed = 100;
    emitter.speedVar = 20;
    
    emitter.startSize = 100.0f;
    emitter.startSizeVar = 1.0f;
    
    emitter.startRadius = 10.0f;
    emitter.endRadius = 100.0f;
    
    //[self addChild:emitter z:500];
    [dogJetPack addChild: emitter z:400];

}

- (CCSpriteFrame *)dogFrame:(int )num isJumpDogi:(BOOL )isJumpDogi {
    
    int frontLegNo = isJumpDogi ? 1 : num;
    
    NSString *avatarBodyStr     = [NSString stringWithFormat:@"dog-body%d-%d.png", _numBody, num];
    NSString *avatarHeadStr     = [NSString stringWithFormat:@"dog-head%d-%d.png", _numHead, num];
    NSString *avatarScarfStr    = [NSString stringWithFormat:@"dog-scarf%d-%d.png", _numScarf, num];
    NSString *avatarEarStr      = [NSString stringWithFormat:@"dog-ear%d-%d.png", _numEar, num];
    NSString *avatarBLegStr     = [NSString stringWithFormat:@"dog-back-leg%d-%d.png", _numbLeg, num];
    NSString *avatarFLegStr     = [NSString stringWithFormat:@"dog-front-leg%d-%d.png", _numfLeg, frontLegNo];
    NSString *avatarTailStr     = [NSString stringWithFormat:@"dog-tail%d-%d.png", _numTail, num];
    
    UIImage *image1 = [UIImage imageNamed:avatarBodyStr];
    UIImage *image2 = [UIImage imageNamed:avatarHeadStr];
    UIImage *image3 = [UIImage imageNamed:avatarScarfStr];
    UIImage *image4 = [UIImage imageNamed:avatarEarStr];
    UIImage *image5 = [UIImage imageNamed:avatarBLegStr];
    UIImage *image6 = [UIImage imageNamed:avatarFLegStr];
    UIImage *image7 = [UIImage imageNamed:avatarTailStr];

    UIGraphicsBeginImageContext( image1.size );
    
    CGRect bodyRect;
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0)) {
        bodyRect = CGRectMake(0, 0, image1.size.width, image1.size.height);
    } else {
        bodyRect = CGRectMake(0, 0, image1.size.width/2, image1.size.height/2);
    }
    
    // Use existing opacity as is
    [image1 drawInRect:bodyRect];
    [image2 drawInRect:bodyRect];
    [image3 drawInRect:bodyRect];
    [image4 drawInRect:bodyRect];
    [image6 drawInRect:bodyRect];
    [image7 drawInRect:bodyRect];
    
    // Apply supplied opacity if applicable
    [image5 drawInRect:bodyRect];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    CCSpriteFrame *spriteFrame1 = [CCSpriteFrame frameWithTexture:[[CCTextureCache sharedTextureCache] addCGImage:newImage.CGImage forKey:nil] rect:CGRectMake(0, 0, 200, 100)];
    
    return spriteFrame1;
}

- (int)selectedIndexDogHeadWithName:(NSString *)name {
    
    if ([name rangeOfString:@"1"].location != NSNotFound) {
        return 1;
    }
    else if ([name rangeOfString:@"2"].location != NSNotFound) {
        return 2;
    }
    else if ([name rangeOfString:@"3"].location != NSNotFound) {
        return 3;
    }
    else if ([name rangeOfString:@"4"].location != NSNotFound) {
        return 4;
    }
    
    return 1;
}

- (void)dealloc {
    [super dealloc];
    
    [_burder release];
    [bHBone release];
    [bHFish release];
    [bHLegPiece release];
}

@end
