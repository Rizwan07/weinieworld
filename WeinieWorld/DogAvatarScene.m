//
//  DogAvatarScene.m
//  WeinieWorld
//
//  Created by Rizwan on 2/27/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "DogAvatarScene.h"
#import "MenuScene.h"
#import "SimpleAudioEngine.h"

@interface DogAvatarScene()<UITextFieldDelegate>

@property (nonatomic, retain) CCLabelTTF *rightMenuHeadinglbl;

@property (nonatomic, retain) CCSprite *menuSprite1;
@property (nonatomic, retain) CCSprite *menuSprite2;
@property (nonatomic, retain) CCSprite *menuSprite3;
@property (nonatomic, retain) CCSprite *menuSprite4;

@property (nonatomic, retain) CCSprite *avatarBodySprite;
@property (nonatomic, retain) CCSprite *avatarBackLegSprite;
@property (nonatomic, retain) CCSprite *avatarFrontLegSprite;
@property (nonatomic, retain) CCSprite *avatarHeadSprite;
@property (nonatomic, retain) CCSprite *avatarScarfSprite;
@property (nonatomic, retain) CCSprite *avatarEarSprite;
@property (nonatomic, retain) CCSprite *avatarTailSprite;

@property (nonatomic, retain) NSString *avatarHeadStr;
@property (nonatomic, retain) NSString *avatarBodyStr;
@property (nonatomic, retain) NSString *avatarEarStr;
@property (nonatomic, retain) NSString *avatarScarfStr;
@property (nonatomic, retain) NSString *avatarBLegStr;
@property (nonatomic, retain) NSString *avatarFLegStr;
@property (nonatomic, retain) NSString *avatarTailStr;

//@property (nonatomic, retain) NSString *txtBgMusicName;

@property (nonatomic, assign) int selectedLeftOption;

@property (nonatomic, retain) UITextField *txtField;

- (void)setDogAssetsForAvatarForScreenSize:(CGSize )size;
- (void)setSoundsWithName:(NSString *)fileName;

@end

@implementation DogAvatarScene

+ (CCScene *)scene {
    
    CCScene *scene = [CCScene node];
    
    DogAvatarScene *avatarScene = [DogAvatarScene node];
    [scene addChild:avatarScene];
    
    return scene;
}

- (id)init {
    
    self = [super init];
    
    if (self != nil) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        CCSprite *backgroundSprite = [CCSprite spriteWithFile:@"bg-avatar.png"];
        [backgroundSprite setPosition:ccp(size.width/2, size.height/2)];
        [self addChild:backgroundSprite];
        
        CCSprite *leftBgSprite = [CCSprite spriteWithFile:@"bg-main-menu.png"];
        [leftBgSprite setPosition:ccp(leftBgSprite.contentSize.width/2, size.height/2)];
        [self addChild:leftBgSprite];
        
        CCSprite *rightBgSprite = [CCSprite spriteWithFile:@"bg-main-menu.png"];
        [rightBgSprite setPosition:ccp(size.width-leftBgSprite.contentSize.width/2, size.height/2)];
        [self addChild:rightBgSprite];

        CCSprite *headerSprite = [CCSprite spriteWithFile:@"header-bar.png"];
        [headerSprite setPosition:ccp(size.width/2, size.height-headerSprite.contentSize.height/2)];
        [self addChild:headerSprite];
        
        if (size.width == 480) {
            headerSprite.scaleX = 0.7;
        }
        
        // left menu
        
        CCMenuItem *leftMenuItem1 = [CCMenuItemImage itemWithNormalImage:@"bg-left-menu-item.png" selectedImage:@"bg-left-menu-item.png" block:^(id sender) {
            
            _selectedLeftOption = 1;
            [_rightMenuHeadinglbl setString:@"Head"];
            
//            CCTexture2D *texture1 = [[CCTextureCache sharedTextureCache] addImage:@"head-1.png"];
//            CCTexture2D *texture2 = [[CCTextureCache sharedTextureCache] addImage:@"head-2.png"];
//            CCTexture2D *texture3 = [[CCTextureCache sharedTextureCache] addImage:@"head-3.png"];
//            CCTexture2D *texture4 = [[CCTextureCache sharedTextureCache] addImage:@"head-4.png"];
            
            [_menuSprite1 setTexture:[[CCSprite spriteWithFile:@"head-1.png"] texture]];
            [_menuSprite2 setTexture:[[CCSprite spriteWithFile:@"head-2.png"] texture]];
            [_menuSprite3 setTexture:[[CCSprite spriteWithFile:@"head-3.png"] texture]];
            [_menuSprite4 setTexture:[[CCSprite spriteWithFile:@"head-4.png"] texture]];

        }];
        
        CCMenuItem *leftMenuItem2 = [CCMenuItemImage itemWithNormalImage:@"bg-left-menu-item.png" selectedImage:@"bg-left-menu-item.png" block:^(id sender) {
            
            _selectedLeftOption = 2;
            [_rightMenuHeadinglbl setString:@"Body"];
            
            [_menuSprite1 setTexture:[[CCSprite spriteWithFile:@"body-1.png"] texture]];
            [_menuSprite2 setTexture:[[CCSprite spriteWithFile:@"body-2.png"] texture]];
            [_menuSprite3 setTexture:[[CCSprite spriteWithFile:@"body-3.png"] texture]];
            [_menuSprite4 setTexture:[[CCSprite spriteWithFile:@"body-4.png"] texture]];
        }];
        
        CCMenuItem *leftMenuItem3 = [CCMenuItemImage itemWithNormalImage:@"bg-left-menu-item.png" selectedImage:@"bg-left-menu-item.png" block:^(id sender) {
            
            _selectedLeftOption = 3;
            [_rightMenuHeadinglbl setString:@"Collar"];
            [_menuSprite1 setTexture:[[CCSprite spriteWithFile:@"collar-1.png"] texture]];
            [_menuSprite2 setTexture:[[CCSprite spriteWithFile:@"collar-2.png"] texture]];
            [_menuSprite3 setTexture:[[CCSprite spriteWithFile:@"collar-3.png"] texture]];
            [_menuSprite4 setTexture:[[CCSprite spriteWithFile:@"collar-4.png"] texture]];

        }];
        
        CCMenuItem *leftMenuItem4 = [CCMenuItemImage itemWithNormalImage:@"bg-left-menu-item.png" selectedImage:@"bg-left-menu-item.png" block:^(id sender) {
            
            _selectedLeftOption = 4;
            [_rightMenuHeadinglbl setString:@"Ear"];
            [_menuSprite1 setTexture:[[CCSprite spriteWithFile:@"ear-1.png"] texture]];
            [_menuSprite2 setTexture:[[CCSprite spriteWithFile:@"ear-2.png"] texture]];
            [_menuSprite3 setTexture:[[CCSprite spriteWithFile:@"ear-3.png"] texture]];
            [_menuSprite4 setTexture:[[CCSprite spriteWithFile:@"ear-4.png"] texture]];

        }];
        
        CCMenuItem *leftMenuItem5 = [CCMenuItemImage itemWithNormalImage:@"bg-left-menu-item.png" selectedImage:@"bg-left-menu-item.png" block:^(id sender) {
            
            _selectedLeftOption = 5;
            [_rightMenuHeadinglbl setString:@"Leg"];
            [_menuSprite1 setTexture:[[CCSprite spriteWithFile:@"leg-1.png"] texture]];
            [_menuSprite2 setTexture:[[CCSprite spriteWithFile:@"leg-2.png"] texture]];
            [_menuSprite3 setTexture:[[CCSprite spriteWithFile:@"leg-3.png"] texture]];
            [_menuSprite4 setTexture:[[CCSprite spriteWithFile:@"leg-4.png"] texture]];

        }];
        
        CCMenuItem *leftMenuItem6 = [CCMenuItemImage itemWithNormalImage:@"bg-left-menu-item.png" selectedImage:@"bg-left-menu-item.png" block:^(id sender) {
            
            _selectedLeftOption = 6;
            [_rightMenuHeadinglbl setString:@"Tail"];
            [_menuSprite1 setTexture:[[CCSprite spriteWithFile:@"tail-1.png"] texture]];
            [_menuSprite2 setTexture:[[CCSprite spriteWithFile:@"tail-2.png"] texture]];
            [_menuSprite3 setTexture:[[CCSprite spriteWithFile:@"tail-3.png"] texture]];
            [_menuSprite4 setTexture:[[CCSprite spriteWithFile:@"tail-4.png"] texture]];

        }];
        
        CCMenu *leftMenu = [CCMenu menuWithItems:leftMenuItem1, leftMenuItem2, leftMenuItem3, leftMenuItem4, leftMenuItem5,leftMenuItem6, nil];
        [leftMenu alignItemsVerticallyWithPadding:5];
        [leftMenu setPosition:ccp(leftBgSprite.contentSize.width/2, size.height/2)];
        
        [self addChild:leftMenu];
        
        int x = leftMenuItem1.contentSize.width;
        int y = leftMenuItem1.contentSize.height/2;
        
        CCSprite *headLblSprite = [CCSprite spriteWithFile:@"head-lbl-small.png"];
        [headLblSprite setPosition:ccp(x - headLblSprite.contentSize.width/2 - 10, y)];
        [leftMenuItem1 addChild:headLblSprite];
        CCSprite *headSprite = [CCSprite spriteWithFile:@"head-of-dog.png"];
        [headSprite setPosition:ccp(headSprite.contentSize.width/2 + 15, y)];
        [leftMenuItem1 addChild:headSprite];
        
        CCSprite *bodyLblSprite = [CCSprite spriteWithFile:@"body-lbl-small.png"];
        [bodyLblSprite setPosition:ccp(x - bodyLblSprite.contentSize.width/2 - 10, y)];
        [leftMenuItem2 addChild:bodyLblSprite];
        CCSprite *bodySprite = [CCSprite spriteWithFile:@"body-of-dog.png"];
        [bodySprite setPosition:ccp(bodySprite.contentSize.width/2 + 15, y)];
        [leftMenuItem2 addChild:bodySprite];
        
        CCSprite *collarLblSprite = [CCSprite spriteWithFile:@"collar-lbl-small.png"];
        [collarLblSprite setPosition:ccp(x - collarLblSprite.contentSize.width/2 - 10, y)];
        [leftMenuItem3 addChild:collarLblSprite];
        CCSprite *collarSprite = [CCSprite spriteWithFile:@"collar-of-dog.png"];
        [collarSprite setPosition:ccp(collarSprite.contentSize.width/2 + 15, y)];
        [leftMenuItem3 addChild:collarSprite];
        
        CCSprite *earLblSprite = [CCSprite spriteWithFile:@"ear-lbl-small.png"];
        [earLblSprite setPosition:ccp(x - earLblSprite.contentSize.width/2 - 10, y)];
        [leftMenuItem4 addChild:earLblSprite];
        CCSprite *earSprite = [CCSprite spriteWithFile:@"ear-of-dog.png"];
        [earSprite setPosition:ccp(earSprite.contentSize.width/2 + 15, y)];
        [leftMenuItem4 addChild:earSprite];
        
        CCSprite *legLblSprite = [CCSprite spriteWithFile:@"leg-lbl-small.png"];
        [legLblSprite setPosition:ccp(x - legLblSprite.contentSize.width/2 - 10, y)];
        [leftMenuItem5 addChild:legLblSprite];
        CCSprite *legSprite = [CCSprite spriteWithFile:@"leg-of-dog.png"];
        [legSprite setPosition:ccp(legSprite.contentSize.width/2 + 15, y)];
        [leftMenuItem5 addChild:legSprite];
        
        CCSprite *tailLblSprite = [CCSprite spriteWithFile:@"tail-lbl-small.png"];
        [tailLblSprite setPosition:ccp(x - tailLblSprite.contentSize.width/2 - 10, y)];
        [leftMenuItem6 addChild:tailLblSprite];
        CCSprite *tailSprite = [CCSprite spriteWithFile:@"tail-of-dog.png"];
        [tailSprite setPosition:ccp(tailLblSprite.contentSize.width/2 + 20, y)];
        [leftMenuItem6 addChild:tailSprite];
        
        // right menu
        
        CCMenuItem *rightMenuItem1 = [CCMenuItemImage itemWithNormalImage:@"bg-right-menu-item.png" selectedImage:@"bg-right-menu-item.png" block:^(id sender) {
            
            
            switch (_selectedLeftOption) {
                case 1: {
                    
                    [_avatarHeadSprite setTexture:[[CCSprite spriteWithFile:@"avatar-head1.png"] texture]];
                    _avatarHeadStr = @"avatar-head1.png";
                    
                }
                    break;
                case 2: {
                    [_avatarBodySprite setTexture:[[CCSprite spriteWithFile:@"avatar-body1.png"] texture]];
                    _avatarBodyStr = @"avatar-body1.png";

                }
                    break;
                case 3: {
                    [_avatarScarfSprite setTexture:[[CCSprite spriteWithFile:@"avatar-scarf1.png"] texture]];
                    _avatarScarfStr = @"avatar-scarf1.png";

                }
                    break;
                case 4: {
                    [_avatarEarSprite setTexture:[[CCSprite spriteWithFile:@"avatar-ear1.png"] texture]];
                    _avatarEarStr = @"avatar-ear1.png";

                }
                    break;
                case 5: {
                    [_avatarBackLegSprite setTexture:[[CCSprite spriteWithFile:@"avatar-back-legs1.png"] texture]];
                    [_avatarFrontLegSprite setTexture:[[CCSprite spriteWithFile:@"avatar-front-legs1.png"] texture]];
                    
                    _avatarBLegStr = @"avatar-back-legs1.png";
                    _avatarFLegStr = @"avatar-front-legs1.png";
                    
                }
                    break;
                case 6: {
                    [_avatarTailSprite setTexture:[[CCSprite spriteWithFile:@"avatar-tail1.png"] texture]];
                    _avatarTailStr = @"avatar-tail1.png";

                }
                    break;
                case 7: {
                    //_txtBgMusicName = @"bg-music-1.mp3";
                    [self setSoundsWithName:@"bg-music-1.mp3"];
                }
                    break;
                default:
                    break;
            }
            
        }];
        
        CCMenuItem *rightMenuItem2 = [CCMenuItemImage itemWithNormalImage:@"bg-right-menu-item.png" selectedImage:@"bg-right-menu-item.png" block:^(id sender) {
            
            switch (_selectedLeftOption) {
                case 1: {
                    [_avatarHeadSprite setTexture:[[CCSprite spriteWithFile:@"avatar-head2.png"] texture]];
                    _avatarHeadStr = @"avatar-head2.png";
                    
                }
                    break;
                case 2: {
                    [_avatarBodySprite setTexture:[[CCSprite spriteWithFile:@"avatar-body2.png"] texture]];
                    _avatarBodyStr = @"avatar-body2.png";

                }
                    break;
                case 3: {
                    [_avatarScarfSprite setTexture:[[CCSprite spriteWithFile:@"avatar-scarf2.png"] texture]];
                    _avatarScarfStr = @"avatar-scarf2.png";

                }
                    break;
                case 4: {
                    [_avatarEarSprite setTexture:[[CCSprite spriteWithFile:@"avatar-ear2.png"] texture]];
                    _avatarEarStr = @"avatar-ear2.png";

                }
                    break;
                case 5: {
                    [_avatarBackLegSprite setTexture:[[CCSprite spriteWithFile:@"avatar-back-legs2.png"] texture]];
                    [_avatarFrontLegSprite setTexture:[[CCSprite spriteWithFile:@"avatar-front-legs2.png"] texture]];
                    
                    _avatarBLegStr = @"avatar-back-legs2.png";
                    _avatarFLegStr = @"avatar-front-legs2.png";

                }
                    break;
                case 6: {
                    [_avatarTailSprite setTexture:[[CCSprite spriteWithFile:@"avatar-tail2.png"] texture]];
                    _avatarTailStr = @"avatar-tail2.png";

                }
                    break;
                case 7: {
                    //_txtBgMusicName = @"bg-music-2.mp3";
                    [self setSoundsWithName:@"bg-music-2.mp3"];
                }
                    break;

                default:
                    break;
            }

        }];
        
        CCMenuItem *rightMenuItem3 = [CCMenuItemImage itemWithNormalImage:@"bg-right-menu-item.png" selectedImage:@"bg-right-menu-item.png" block:^(id sender) {
            
            switch (_selectedLeftOption) {
                case 1: {
                    [_avatarHeadSprite setTexture:[[CCSprite spriteWithFile:@"avatar-head3.png"] texture]];
                    _avatarHeadStr = @"avatar-head3.png";
                    
                }
                    break;
                case 2: {
                    [_avatarBodySprite setTexture:[[CCSprite spriteWithFile:@"avatar-body3.png"] texture]];
                    _avatarBodyStr = @"avatar-body3.png";

                }
                    break;
                case 3: {
                    [_avatarScarfSprite setTexture:[[CCSprite spriteWithFile:@"avatar-scarf3.png"] texture]];
                    _avatarScarfStr = @"avatar-scarf3.png";

                }
                    break;
                case 4: {
                    [_avatarEarSprite setTexture:[[CCSprite spriteWithFile:@"avatar-ear3.png"] texture]];
                    _avatarEarStr = @"avatar-ear3.png";

                }
                    break;
                case 5: {
                    [_avatarBackLegSprite setTexture:[[CCSprite spriteWithFile:@"avatar-back-legs3.png"] texture]];
                    [_avatarFrontLegSprite setTexture:[[CCSprite spriteWithFile:@"avatar-front-legs3.png"] texture]];
                    
                    _avatarBLegStr = @"avatar-back-legs3.png";
                    _avatarFLegStr = @"avatar-front-legs3.png";

                }
                    break;
                case 6: {
                    [_avatarTailSprite setTexture:[[CCSprite spriteWithFile:@"avatar-tail3.png"] texture]];
                    _avatarTailStr = @"avatar-tail3.png";

                }
                    break;
                case 7: {
                    //_txtBgMusicName = @"bg-music-3.mp3";
                    [self setSoundsWithName:@"bg-music-3.mp3"];
                }
                    break;

                default:
                    break;
            }

        }];
        
        CCMenuItem *rightMenuItem4 = [CCMenuItemImage itemWithNormalImage:@"bg-right-menu-item.png" selectedImage:@"bg-right-menu-item.png" block:^(id sender) {
            
            switch (_selectedLeftOption) {
                case 1: {
                    [_avatarHeadSprite setTexture:[[CCSprite spriteWithFile:@"avatar-head4.png"] texture]];
                    _avatarHeadStr = @"avatar-head4.png";
                    
                }
                    break;
                case 2: {
                    [_avatarBodySprite setTexture:[[CCSprite spriteWithFile:@"avatar-body4.png"] texture]];
                    _avatarBodyStr = @"avatar-body4.png";

                }
                    break;
                case 3: {
                    [_avatarScarfSprite setTexture:[[CCSprite spriteWithFile:@"avatar-scarf4.png"] texture]];
                    _avatarScarfStr = @"avatar-scarf4.png";

                }
                    break;
                case 4: {
                    [_avatarEarSprite setTexture:[[CCSprite spriteWithFile:@"avatar-ear4.png"] texture]];
                    _avatarEarStr = @"avatar-ear4.png";

                }
                    break;
                case 5: {
                    [_avatarBackLegSprite setTexture:[[CCSprite spriteWithFile:@"avatar-back-legs4.png"] texture]];
                    [_avatarFrontLegSprite setTexture:[[CCSprite spriteWithFile:@"avatar-front-legs4.png"] texture]];
                    
                    _avatarBLegStr = @"avatar-back-legs4.png";
                    _avatarFLegStr = @"avatar-front-legs4.png";
                    
                }
                    break;
                case 6: {
                    [_avatarTailSprite setTexture:[[CCSprite spriteWithFile:@"avatar-tail4.png"] texture]];
                    _avatarTailStr = @"avatar-tail4.png";

                }
                    break;
                case 7: {
                    //_txtBgMusicName = @"bg-music-4.mp3";
                    [self setSoundsWithName:@"bg-music-4.mp3"];
                }
                    break;

                default:
                    break;
            }

        }];
        
        CCMenu *rightMenu = [CCMenu menuWithItems:rightMenuItem1, rightMenuItem2, rightMenuItem3, rightMenuItem4, nil];
        [rightMenu alignItemsVerticallyWithPadding:5];
        [rightMenu setPosition:ccp(size.width-leftBgSprite.contentSize.width/2, size.height/2-20)];
        
        [self addChild:rightMenu];
        
        
        _menuSprite1 = [CCSprite spriteWithTexture:[[CCSprite spriteWithFile:@"head-1.png"]texture]];
        [_menuSprite1 setPosition:ccp(rightMenuItem1.contentSize.width/2, rightMenuItem1.contentSize.height/2)];
        [rightMenuItem1 addChild:_menuSprite1];
        
        _menuSprite2 = [CCSprite spriteWithTexture:[[CCSprite spriteWithFile:@"head-2.png"]texture]];
        [_menuSprite2 setPosition:ccp(rightMenuItem1.contentSize.width/2, rightMenuItem1.contentSize.height/2)];
        [rightMenuItem2 addChild:_menuSprite2];
        
        _menuSprite3 = [CCSprite spriteWithTexture:[[CCSprite spriteWithFile:@"head-3.png"]texture]];
        [_menuSprite3 setPosition:ccp(rightMenuItem1.contentSize.width/2, rightMenuItem1.contentSize.height/2)];
        [rightMenuItem3 addChild:_menuSprite3];
        
        _menuSprite4 = [CCSprite spriteWithTexture:[[CCSprite spriteWithFile:@"head-4.png"]texture]];
        [_menuSprite4 setPosition:ccp(rightMenuItem1.contentSize.width/2, rightMenuItem1.contentSize.height/2)];
        [rightMenuItem4 addChild:_menuSprite4];
        
        _rightMenuHeadinglbl = [CCLabelTTF labelWithString:@"Head" fontName:@"Helvetica-Bold" fontSize:20];
        [_rightMenuHeadinglbl setPosition:ccp(size.width - rightBgSprite.contentSize.width/2, size.height - _rightMenuHeadinglbl.contentSize.height*1.5)];
        [self addChild:_rightMenuHeadinglbl];
        
        
        // header menu
        
        CCMenuItem *headerHomeMenuItem = [CCMenuItemImage itemWithNormalImage:@"home-btn.png" selectedImage:@"home-btn.png" block:^(id sender) {
            
            [_txtField removeFromSuperview];
            [_txtField release];
            
            
            CCScene *layer = [CCTransitionCrossFade transitionWithDuration:1.0 scene:[MenuScene scene]];
            [[CCDirector sharedDirector] replaceScene:layer];

        }];
        
        CCMenuItem *headerDoneMenuItem = [CCMenuItemImage itemWithNormalImage:@"done-btn.png" selectedImage:@"done-btn.png" block:^(id sender) {
            
            [_txtField removeFromSuperview];
            [_txtField release];
            
            [defaults setObject:_avatarHeadStr forKey:@"avatarHead"];
            [defaults setObject:_avatarBodyStr forKey:@"avatarBody"];
            [defaults setObject:_avatarEarStr forKey:@"avatarEar"];
            [defaults setObject:_avatarScarfStr forKey:@"avatarScarf"];
            [defaults setObject:_avatarBLegStr forKey:@"avatarBLeg"];
            [defaults setObject:_avatarFLegStr forKey:@"avatarFLeg"];
            [defaults setObject:_avatarTailStr forKey:@"avatarTail"];
            //[defaults setObject:_txtBgMusicName forKey:@"bgMusic"];
            [defaults synchronize];
            
            CCScene *layer = [CCTransitionCrossFade transitionWithDuration:1.0 scene:[MenuScene scene]];
            [[CCDirector sharedDirector] replaceScene:layer];

        }];
        
        CCMenu *headerMenu = [CCMenu menuWithItems:headerHomeMenuItem, headerDoneMenuItem, nil];
        [headerMenu alignItemsHorizontallyWithPadding:size.width-400];
        [headerMenu setPosition:ccp(size.width/2, size.height-headerSprite.contentSize.height/4)];
        
        [self addChild:headerMenu];
        
        [self setDogAssetsForAvatarForScreenSize:size];
        
        CCSprite *txtFieldBgSprite = [CCSprite spriteWithFile:@"bg-textfield.png"];
        [txtFieldBgSprite setPosition:ccp(rightBgSprite.contentSize.width + txtFieldBgSprite.contentSize.width/2,
                                          size.height - (headerSprite.contentSize.height/2 + headerSprite.contentSize.height/4)+4)];
        [self addChild:txtFieldBgSprite];
        
        self.txtField = [[UITextField alloc] initWithFrame:CGRectMake(rightBgSprite.contentSize.width + 4,
                                                                      headerSprite.contentSize.height/2+headerSprite.contentSize.height/4-12,
                                                                      110, 20)];
        [_txtField setDelegate:self];
        [_txtField setPlaceholder:@"Name your Weinie"];
        [_txtField setText:[defaults objectForKey:@"nameDogi"]];
        [_txtField setFont:[UIFont systemFontOfSize:13]];
        [_txtField setTextColor:[UIColor whiteColor]];
        [[[CCDirector sharedDirector] view] addSubview:_txtField];
        
        CCMenuItem *menuItemSound = [CCMenuItemImage itemWithNormalImage:@"break-your-sound.png" selectedImage:@"break-your-sound.png" block:^(id sender) {
            
            _selectedLeftOption = 7;
            [_rightMenuHeadinglbl setString:@"Bark Sounds"];
            [_menuSprite1 setTexture:[[CCSprite spriteWithFile:@"bg-music-1.png"] texture]];
            [_menuSprite2 setTexture:[[CCSprite spriteWithFile:@"bg-music-2.png"] texture]];
            [_menuSprite3 setTexture:[[CCSprite spriteWithFile:@"bg-music-3.png"] texture]];
            [_menuSprite4 setTexture:[[CCSprite spriteWithFile:@"bg-music-4.png"] texture]];

        }];
        CCMenu *soundMenu = [CCMenu menuWithItems:menuItemSound, nil];
        [soundMenu setPosition:ccp(size.width-(rightBgSprite.contentSize.width)-menuItemSound.contentSize.width/2, size.height - (headerSprite.contentSize.height/2 + headerSprite.contentSize.height/4)+3)];
        //[soundMenu setPosition:ccp(size.width/2+95, size.height - (headerSprite.contentSize.height/2 + headerSprite.contentSize.height/4)+3)];
        [self addChild:soundMenu];
        
        _selectedLeftOption = 1;
    }
    
    return self;
}

- (void)setDogAssetsForAvatarForScreenSize:(CGSize)size {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _avatarHeadStr = [defaults objectForKey:@"avatarHead"];
    _avatarBodyStr = [defaults objectForKey:@"avatarBody"];
    _avatarEarStr = [defaults objectForKey:@"avatarEar"];
    _avatarScarfStr = [defaults objectForKey:@"avatarScarf"];
    _avatarBLegStr = [defaults objectForKey:@"avatarBLeg"];
    _avatarFLegStr = [defaults objectForKey:@"avatarFLeg"];
    _avatarTailStr = [defaults objectForKey:@"avatarTail"];
    //_txtBgMusicName = [defaults objectForKey:@"bgMusic"];
    
    _avatarHeadSprite = [CCSprite spriteWithTexture:[[CCSprite spriteWithFile:_avatarHeadStr] texture]];//[CCSprite spriteWithFile:avatarHead];
    [_avatarHeadSprite setPosition:ccp(size.width/2, size.height/2 - 60)];
    [self addChild:_avatarHeadSprite];
    
    _avatarBodySprite = [CCSprite spriteWithTexture:[[CCSprite spriteWithFile:_avatarBodyStr] texture]];//[CCSprite spriteWithFile:avatarBody];
    [_avatarBodySprite setPosition:ccp(size.width/2, size.height/2 - 60)];
    [self addChild:_avatarBodySprite];
    
    _avatarBackLegSprite = [CCSprite spriteWithTexture:[[CCSprite spriteWithFile:_avatarBLegStr] texture]];//[CCSprite spriteWithFile:avatarBLeg];
    [_avatarBackLegSprite setPosition:ccp(size.width/2, size.height/2 - 60)];
    [self addChild:_avatarBackLegSprite];
    
    _avatarFrontLegSprite = [CCSprite spriteWithTexture:[[CCSprite spriteWithFile:_avatarFLegStr] texture]];//[CCSprite spriteWithFile:avatarFLeg];
    [_avatarFrontLegSprite setPosition:ccp(size.width/2, size.height/2 - 60)];
    [self addChild:_avatarFrontLegSprite];
    
    _avatarTailSprite = [CCSprite spriteWithTexture:[[CCSprite spriteWithFile:_avatarTailStr] texture]];//[CCSprite spriteWithFile:avatarTail];
    [_avatarTailSprite setPosition:ccp(size.width/2, size.height/2 - 60)];
    [self addChild:_avatarTailSprite];
    
    _avatarScarfSprite = [CCSprite spriteWithTexture:[[CCSprite spriteWithFile:_avatarScarfStr] texture]];//[CCSprite spriteWithFile:avatarScarf];
    [_avatarScarfSprite setPosition:ccp(size.width/2, size.height/2 - 60)];
    [self addChild:_avatarScarfSprite];
    
    _avatarEarSprite = [CCSprite spriteWithTexture:[[CCSprite spriteWithFile:_avatarEarStr] texture]];//[CCSprite spriteWithFile:avatarEar];
    [_avatarEarSprite setPosition:ccp(size.width/2, size.height/2 - 60)];
    [self addChild:_avatarEarSprite];

    if (size.width == 480) {
        [_avatarHeadSprite setScale:0.7];
        [_avatarBodySprite setScale:0.7];
        [_avatarBackLegSprite setScale:0.7];
        [_avatarFrontLegSprite setScale:0.7];
        [_avatarTailSprite setScale:0.7];
        [_avatarScarfSprite setScale:0.7];
        [_avatarEarSprite setScale:0.7];
    }
    
}

- (void)setSoundsWithName:(NSString *)fileName {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    SimpleAudioEngine *audioEngine = [SimpleAudioEngine sharedEngine];
    
    //[audioEngine stopBackgroundMusic];
    if ([defaults boolForKey:@"isSoundOn"]) {
        
        [audioEngine playBackgroundMusic:fileName];
    }
    
    [defaults setObject:fileName forKey:@"bgMusic"];
    [defaults synchronize];
    
}

#pragma mark --
#pragma mark UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [[NSUserDefaults standardUserDefaults] setObject:textField.text forKey:@"nameDogi"];
    
    [textField resignFirstResponder];
    return YES;
}

@end
